package id.bootcamp.card.ui.screen.play.modifiers

import org.junit.jupiter.api.Assertions.*

import org.junit.jupiter.api.Test

class CardSizeModifierTest {

    @Test
    fun calcResponsivePlayingCardSize_defaultScreen_cardHeightLongerThanWidth() {
        val (width, height) = calcResponsivePlayingCardSize(480, 900)
        assertEquals(true, width < height)
    }

    @Test
    fun calcResponsivePlayingCardSize_defaultScreen_correctWidthHeightRatio() {
        val (width, height) = calcResponsivePlayingCardSize(480, 900)
        assertEquals(60/90.0, width/height.toDouble())
    }

    @Test
    fun calcResponsivePlayingCardSize_thinPortraitScreen_correctWidthHeightRatio() {
        val (width, height) = calcResponsivePlayingCardSize(400, 900)
        assertEquals(60/90.0, width/height.toDouble())
    }

    @Test
    fun calcResponsivePlayingCardSize_landscapeScreen_cardHeightLongerThanWidth() {
        val (width, height) = calcResponsivePlayingCardSize(900, 480)
        assertEquals(true, width < height)
    }

    @Test
    fun calcResponsivePlayingCardSize_landscapeScreen_correctWidthHeightRatio() {
        val (width, height) = calcResponsivePlayingCardSize(900, 480)
        assertEquals(60/90.0, width/height.toDouble())
    }

    @Test
    fun calcResponsivePlayingCardSize_thinLandscapeScreen_correctWidthHeightRatio() {
        val (width, height) = calcResponsivePlayingCardSize(900, 400)
        assertEquals(60/90.0, width/height.toDouble())
    }

}