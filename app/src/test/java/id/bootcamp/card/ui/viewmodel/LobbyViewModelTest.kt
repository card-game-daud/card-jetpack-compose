package id.bootcamp.card.ui.viewmodel

import androidx.arch.core.executor.ArchTaskExecutor
import androidx.arch.core.executor.TaskExecutor
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.snapshots.SnapshotStateList
import id.bootcamp.card.domain.model.AnonymousUserModel
import id.bootcamp.card.domain.model.LobbyGameModel
import id.bootcamp.card.domain.repository.AuthRepositoryFake
import id.bootcamp.card.domain.repository.AuthRepository
import id.bootcamp.card.domain.repository.LobbyRepository
import id.bootcamp.card.domain.repository.LobbyRepositoryFake
import id.bootcamp.card.domain.usecase.EnterLobbyUseCase
import id.bootcamp.card.domain.usecase.GetAnonymousUserUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.setMain
import org.junit.Rule
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class LobbyViewModelTest {
    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    // Fake repository for testing the view model
    private lateinit var lobbyRepository: LobbyRepository
    private lateinit var authRepository: AuthRepository

    // View model instance for testing
    private lateinit var viewModel: LobbyViewModel

    @BeforeEach
    fun setupViewModel() {
        ArchTaskExecutor.getInstance().setDelegate(object: TaskExecutor(){
            override fun executeOnDiskIO(runnable: Runnable) = runnable.run()

            override fun postToMainThread(runnable: Runnable) = runnable.run()

            override fun isMainThread(): Boolean = true
        })

        lobbyRepository = LobbyRepositoryFake()
        authRepository = AuthRepositoryFake()
        val getAnonymousUserUseCase = GetAnonymousUserUseCase(authRepository)
        val enterLobbyUseCase = EnterLobbyUseCase(getAnonymousUserUseCase, lobbyRepository)
        viewModel = LobbyViewModel(lobbyRepository, enterLobbyUseCase)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun enterLobby_anyCase_userNotNull() {
        Dispatchers.setMain(StandardTestDispatcher())

        viewModel.enterLobby()

        viewModel.userData.observeForever {  }

        assertNotNull(viewModel.userData.value)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun enterLobby_anyCase_lobbyNotNull() {
        Dispatchers.setMain(StandardTestDispatcher())

        viewModel.enterLobby()

        viewModel.lobbyGameData.observeForever {  }

        assertNotNull(viewModel.lobbyGameData.value)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun enterLobby_anyCase_isConnectedNotNull() {
        Dispatchers.setMain(StandardTestDispatcher())

        viewModel.enterLobby()

        viewModel.isConnected.observeForever {  }

        assertNotNull(viewModel.isConnected.value)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun addGame_emptyLobby_lobbyGameDataSizeIncrease() {
        Dispatchers.setMain(StandardTestDispatcher())

        viewModel.enterLobby()

        val user = AnonymousUserModel("user_A", "A")
        val originalGameList = SnapshotStateList<LobbyGameModel>()
        viewModel.lobbyGameData.postValue(originalGameList)

        viewModel.addGame(user)

        viewModel.lobbyGameData.observeForever {  }
        assertEquals(originalGameList.size + 1, viewModel.lobbyGameData.value?.size)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun addGame_nonEmptyLobby_lobbyGameDataSizeIncrease() {
        Dispatchers.setMain(StandardTestDispatcher())

        val user = AnonymousUserModel("user_A", "A")
        val game1 = LobbyGameModel("1", true, listOf(AnonymousUserModel("user_C", "C")), false)
        val originalGameList = mutableStateListOf(game1)
        viewModel.lobbyGameData.postValue(originalGameList)

        viewModel.addGame(user)

        viewModel.lobbyGameData.observeForever {  }

        assertEquals(originalGameList.size + 1, viewModel.lobbyGameData.value?.size)
    }

    @Test
    fun checkIsUserInGame_emptyLobby_returnFalse() {
        val result = viewModel.checkIsUserInGame("", SnapshotStateList())
        assertFalse(result)
    }

    @Test
    fun checkIsUserInGame_notEntered_returnFalse() {
        val game1 = LobbyGameModel("1", true, listOf(AnonymousUserModel("user_C", "C")), false)
        val game2 = LobbyGameModel("2", true, listOf(AnonymousUserModel("user_B", "B")), false)
        val gameList = mutableStateListOf(game1, game2)

        val result = viewModel.checkIsUserInGame("user_A", gameList)
        assertFalse(result)
    }

    @Test
    fun checkIsUserInGame_hasEntered_returnTrue() {
        val game1 = LobbyGameModel("1", true, listOf(AnonymousUserModel("user_A", "A")), false)
        val game2 = LobbyGameModel("2", true, listOf(AnonymousUserModel("user_B", "B")), false)
        val gameList = mutableStateListOf(game1, game2)

        val result = viewModel.checkIsUserInGame("user_A", gameList)
        assertTrue(result)
    }
}