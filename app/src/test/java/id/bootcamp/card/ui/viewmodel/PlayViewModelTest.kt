package id.bootcamp.card.ui.viewmodel

import androidx.arch.core.executor.ArchTaskExecutor
import androidx.arch.core.executor.TaskExecutor
import id.bootcamp.card.domain.model.CardModel
import id.bootcamp.card.domain.model.PlayerDeck
import id.bootcamp.card.domain.repository.AuthRepository
import id.bootcamp.card.domain.repository.AuthRepositoryFake
import id.bootcamp.card.domain.repository.OnlinePlayRepository
import id.bootcamp.card.domain.repository.LobbyRepository
import id.bootcamp.card.domain.repository.LobbyRepositoryFake
import id.bootcamp.card.domain.repository.OnlinePlayRepositoryFake
import id.bootcamp.card.domain.usecase.CalculateDeckScoreUseCase
import id.bootcamp.card.domain.usecase.ComputerMoveUseCase
import id.bootcamp.card.domain.usecase.EndOnlineGameUseCase
import id.bootcamp.card.domain.usecase.EnterMultiPlayerGameUseCase
import id.bootcamp.card.domain.usecase.GenerateDeckUseCase
import id.bootcamp.card.domain.usecase.GeneratePlayersArrayUseCase
import id.bootcamp.card.domain.usecase.GetAnonymousUserUseCase
import id.bootcamp.card.domain.usecase.StartMultiPlayerGameUseCase
import id.bootcamp.card.domain.usecase.StartOnlineGameUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.StandardTestDispatcher
import kotlinx.coroutines.test.setMain
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class PlayViewModelTest {
    // Fake repositories for view-model dependencies
    private lateinit var onlinePlayRepository: OnlinePlayRepository
    private lateinit var authRepository: AuthRepository
    private lateinit var lobbyRepository: LobbyRepository

    // View-models to be tasted
    private lateinit var offlineVM: OfflinePlayViewModel
    private lateinit var onlineVM: OnlinePlayViewModel
    private lateinit var multiplayerVM: MultiPlayerViewModel

    companion object {
        // Constants
        const val NORMAL_DECK_SIZE = 4 // Deck can have 5 cards before discarding; any other size is invalid
    }

    @BeforeEach
    fun setup() {
        // Setup task executor to allow LiveData-observing
        ArchTaskExecutor.getInstance().setDelegate(object: TaskExecutor(){
            override fun executeOnDiskIO(runnable: Runnable) = runnable.run()

            override fun postToMainThread(runnable: Runnable) = runnable.run()

            override fun isMainThread(): Boolean = true
        })

        // Set up dependencies
        onlinePlayRepository = OnlinePlayRepositoryFake()
        authRepository = AuthRepositoryFake()
        lobbyRepository = LobbyRepositoryFake()
        val getAnonymousUserUseCase = GetAnonymousUserUseCase(authRepository)
        val generatePlayersArrayUseCase = GeneratePlayersArrayUseCase()
        val generateDeckUseCase = GenerateDeckUseCase()
        val computerMoveUseCase = ComputerMoveUseCase()
        val calculateDeckScoreUseCase = CalculateDeckScoreUseCase()
        val startOnlineGameUseCase = StartOnlineGameUseCase(onlinePlayRepository, generateDeckUseCase, getAnonymousUserUseCase)
        val endOnlineGameUseCase = EndOnlineGameUseCase(onlinePlayRepository, calculateDeckScoreUseCase)
        val startMultiPlayerGameUseCase = StartMultiPlayerGameUseCase(lobbyRepository, startOnlineGameUseCase)
        val enterMultiPlayerGameUseCase = EnterMultiPlayerGameUseCase(onlinePlayRepository, lobbyRepository)

        // Set up view-models
        offlineVM = OfflinePlayViewModel(
            getAnonymousUserUseCase, generatePlayersArrayUseCase, generateDeckUseCase, computerMoveUseCase
        )
        onlineVM = OnlinePlayViewModel(
            onlinePlayRepository,
            getAnonymousUserUseCase,
            generatePlayersArrayUseCase,
            startOnlineGameUseCase,
            endOnlineGameUseCase,
            computerMoveUseCase
        )
        multiplayerVM = MultiPlayerViewModel(
            onlinePlayRepository,
            lobbyRepository,
            startMultiPlayerGameUseCase,
            enterMultiPlayerGameUseCase,
            generatePlayersArrayUseCase,
            endOnlineGameUseCase,
            computerMoveUseCase
        )

        runBlocking {
            offlineVM.startGame()
        }
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun drawFromDrawPile_offline_deckHasFiveCards() {
        Dispatchers.setMain(StandardTestDispatcher())

        val userId = 0
        val drawnCard = CardModel.ACE_OF_CLUBS
        val deck = PlayerDeck(userId, List(NORMAL_DECK_SIZE) { CardModel.ACE_OF_CLUBS })

        offlineVM.drawFromDrawPile(deck, drawnCard)
        offlineVM.playersDeck.observeForever {  }

        assertTrue(offlineVM.playersDeck.value?.get(userId)?.listCard?.size!! == 5)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun drawFromDrawPile_offline_deckCannotHaveMoreThanFiveCards() {
        Dispatchers.setMain(StandardTestDispatcher())

        val userId = 0
        val drawnCard = CardModel.ACE_OF_CLUBS
        val deck = PlayerDeck(userId, List(NORMAL_DECK_SIZE + 1) { CardModel.ACE_OF_CLUBS })

        offlineVM.drawFromDrawPile(deck, drawnCard)
        offlineVM.playersDeck.observeForever {  }

        assertTrue(offlineVM.playersDeck.value?.get(userId)?.listCard?.size!! <= 5)
    }
}