package id.bootcamp.card.domain.repository

import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.snapshots.SnapshotStateList
import id.bootcamp.card.domain.model.AnonymousUserModel
import id.bootcamp.card.domain.model.CardModel
import id.bootcamp.card.domain.model.GameStatus
import id.bootcamp.card.domain.model.LobbyGameModel
import id.bootcamp.card.domain.model.PlayGameModel
import id.bootcamp.card.domain.model.PlayerDeck
import id.bootcamp.card.domain.model.PlayerDiscardPile
import id.bootcamp.card.domain.model.TurnStatus

class OnlinePlayRepositoryFake: OnlinePlayRepository {
    private val ongoingGames = mutableMapOf<String, CompleteGameModel>()
    private var currentGameId: String? = null

    companion object {
        const val PLAYER_COUNT = 4
    }

    private class CompleteGameModel(
        var gameData: PlayGameModel,
        val userData: Array<AnonymousUserModel?>,
        val enteredUsers: MutableSet<String> // String being the user's id
    ) {
        fun updateGameData(
            updatedDrawPile: SnapshotStateList<CardModel> = this.gameData.drawPile,
            updatedPlayersDeck: Array<PlayerDeck> = this.gameData.playersDecks,
            updatedDiscardPiles: Array<PlayerDiscardPile> = this.gameData.playersDiscardPiles,
            updatedTurnStatuses: Array<TurnStatus> = this.gameData.playersTurnStatuses,
            updatedGameStatus: GameStatus? = this.gameData.gameStatus,
            updatedScores: Array<Int> = this.gameData.playersScores,
            updatedCurrentPlayerId: Int? = this.gameData.currentPlayerId
        ) {
            val updatedGameData = PlayGameModel(
                drawPile = updatedDrawPile,
                playersDecks = updatedPlayersDeck,
                playersDiscardPiles = updatedDiscardPiles,
                playersTurnStatuses = updatedTurnStatuses,
                gameStatus = updatedGameStatus,
                playersScores = updatedScores,
                currentPlayerId = updatedCurrentPlayerId,
            )
            gameData = updatedGameData
        }
    }

    override fun syncGameData(gameId: String, callback: (PlayGameModel) -> Unit) {
        val game = ongoingGames[gameId]
        if (game != null) {
            callback(game.gameData)
        }
    }

    override fun checkConnectionStatus(callback: (Boolean) -> Unit) {
        callback(true)
    }

    override suspend fun startGame(
        lobbyGameData: LobbyGameModel?,
        startingDrawPile: List<CardModel>,
        startingPlayersDecks: Array<PlayerDeck>,
        playerIdRange: IntRange
    ): PlayGameModel {
        val startingPlayerId = playerIdRange.random()
        val gameData = PlayGameModel(
            currentPlayerId = startingPlayerId,
            drawPile = mutableStateListOf(*startingDrawPile.toTypedArray()),
            playersDecks = startingPlayersDecks,
            playersDiscardPiles = Array(PLAYER_COUNT) { PlayerDiscardPile(it, mutableStateListOf()) },
            playersTurnStatuses = Array(PLAYER_COUNT) { if (it == startingPlayerId) TurnStatus.DRAWING else TurnStatus.WAITING },
            gameStatus = GameStatus.ONGOING,
            playersScores = Array(PLAYER_COUNT) { 0 },
        )
        val users = Array(PLAYER_COUNT) { lobbyGameData?.listPlayers?.getOrNull(it) }
        val newGameId = ongoingGames.size.toString()

        currentGameId = newGameId
        ongoingGames[newGameId] = CompleteGameModel(gameData, users, mutableSetOf())

        return gameData
    }

    override fun enterGame(gameId: String, userId: String, callback: (Int) -> Unit) {
        if (gameId.isNotEmpty()) {
            currentGameId = gameId
            val game = ongoingGames[gameId]
            if (game != null) {
                game.enteredUsers.add(userId)
                callback(game.enteredUsers.size)
            }
        }
    }

    override fun drawFromDrawPile(
        updatedPlayersDeck: Array<PlayerDeck>,
        updatedDrawPile: SnapshotStateList<CardModel>,
        updatedPlayerId: Int,
        onFinish: () -> Unit
    ) {
        val game = ongoingGames[currentGameId]
        if (game != null) {
            val updatedTurnStatuses = game.gameData.playersTurnStatuses
            updatedTurnStatuses[updatedPlayerId] = TurnStatus.DISCARDING
            ongoingGames[currentGameId]?.updateGameData(
                updatedDrawPile = updatedDrawPile,
                updatedPlayersDeck = updatedPlayersDeck,
                updatedTurnStatuses = updatedTurnStatuses
            )
        }
    }

    override fun drawFromDiscardPile(
        updatedPlayersDeck: Array<PlayerDeck>,
        updatedDiscardPiles: Array<PlayerDiscardPile>,
        updatedPlayerId: Int,
        onFinish: () -> Unit
    ) {
        val game = ongoingGames[currentGameId]
        if (game != null) {
            val updatedTurnStatuses = game.gameData.playersTurnStatuses
            updatedTurnStatuses[updatedPlayerId] = TurnStatus.DISCARDING
            ongoingGames[currentGameId]?.updateGameData(
                updatedPlayersDeck = updatedPlayersDeck,
                updatedDiscardPiles = updatedDiscardPiles,
                updatedTurnStatuses = updatedTurnStatuses
            )
        }
    }

    override fun discardCard(
        updatedPlayersDeck: Array<PlayerDeck>,
        updatedDiscardPiles: Array<PlayerDiscardPile>,
        onFinish: () -> Unit
    ) {
        val game = ongoingGames[currentGameId]
        if (game != null) {
            ongoingGames[currentGameId]?.updateGameData(
                updatedPlayersDeck = updatedPlayersDeck,
                updatedDiscardPiles = updatedDiscardPiles,
            )
        }
    }

    override fun endTurn(currentPlayerId: Int, callback: () -> Unit) {
        val game = ongoingGames[currentGameId]
        if (game != null) {
            val newCurrentPlayerId = (currentPlayerId + 1) % 4
            ongoingGames[currentGameId]?.updateGameData(
                updatedCurrentPlayerId = newCurrentPlayerId,
                updatedTurnStatuses = Array(4) {if (it == newCurrentPlayerId) {
                    TurnStatus.DRAWING
                } else {
                    TurnStatus.WAITING
                }},
            )
        }
    }

    override fun updateGameStatus(newGameStatus: GameStatus) {
        val game = ongoingGames[currentGameId]
        game?.updateGameData(updatedGameStatus = newGameStatus)
    }

    override fun endGame(gameId: String, scores: Array<Int>) {
        val game = ongoingGames[currentGameId]
        if (game != null) {
            ongoingGames[currentGameId]?.updateGameData(
                updatedGameStatus = GameStatus.FINISHING,
                updatedScores = scores,
            )
        }
    }

    override fun leaveGame(gameId: String, userId: String) {
        val game = ongoingGames[currentGameId]
        if (game != null) {
            if (game.enteredUsers.size <= 1) {
                ongoingGames.remove(currentGameId)
            } else {
                game.enteredUsers.remove(userId)
            }
        }
    }

    override suspend fun getCurrentGameOfUser(userId: String): LobbyGameModel? {
        val potentialGames = ongoingGames.filter {
                game -> game.value.userData.any { user -> user?.id == userId }
        }
        val currentGame = potentialGames.toList().firstOrNull()
        return if (currentGame != null) LobbyGameModel(
            currentGame.first,
            false,
            currentGame.second.userData.filterNotNull(),
            true
        ) else null
    }

    override suspend fun getGameById(gameId: String): LobbyGameModel {
        val game = ongoingGames[gameId]
        return LobbyGameModel(
            if (game != null) gameId else LobbyGameModel.NONE_ID,
            false,
            game?.userData?.filterNotNull() ?: emptyList(),
            true
        )
    }
}