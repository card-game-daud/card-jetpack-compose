package id.bootcamp.card.domain.repository

import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.lifecycle.MutableLiveData
import id.bootcamp.card.domain.model.AnonymousUserModel
import id.bootcamp.card.domain.model.LobbyGameModel

class LobbyRepositoryFake: LobbyRepository {
    private val fakeGameList = MutableLiveData<SnapshotStateList<LobbyGameModel>>(SnapshotStateList())

    companion object {
        private const val hostId = 0
    }

    override fun getLobbyData(callback: (SnapshotStateList<LobbyGameModel>) -> Unit) {
        fakeGameList.observeForever(callback)
    }

    override fun checkConnectionStatus(callback: (Boolean) -> Unit) {
        TODO("Not yet implemented")
    }

    override fun addGame(user: AnonymousUserModel) {
        val newGameList = fakeGameList.value!!
        newGameList.add(
            LobbyGameModel(newGameList.size.toString(), true, listOf(user), false)
        )
        fakeGameList.postValue(newGameList)
    }

    override fun joinGame(user: AnonymousUserModel, gameId: String) {
        val newGameList = fakeGameList.value!!
        val game = newGameList.find { game -> game.id == gameId }
        if (game != null) {
            val index = newGameList.indexOf(game)
            newGameList[index].listPlayers.add(user)
        }
        fakeGameList.postValue(newGameList)
    }

    override fun leaveGame(userId: String, gameId: String) {
        val newGameList = fakeGameList.value!!
        val game = newGameList.find { game -> game.id == gameId }
        if (game != null) {
            val gameIndex = newGameList.indexOf(game)
            val user = game.listPlayers.find { user -> user.id == userId }
            val userIndex = game.listPlayers.indexOf(user)
            if (userIndex == hostId) {
                newGameList.remove(game)
            } else {
                newGameList[gameIndex].listPlayers.remove(user)
            }
        }
        fakeGameList.postValue(newGameList)
    }

    override fun startGame(gameId: String) {
        val newGameList = fakeGameList.value!!
        val game = newGameList.find { game -> game.id == gameId }
        if (game != null) {
            val index = newGameList.indexOf(game)
            newGameList[index].isWaiting = false
        }
        fakeGameList.postValue(newGameList)
    }

    override suspend fun getGameById(id: String): LobbyGameModel? {
        return fakeGameList.value?.find { game -> game.id == id }
    }

    override fun removeGame(gameId: String) {
        val newGameList = fakeGameList.value!!
        val game = newGameList.find { game -> game.id == gameId }
        if (game != null) {
            newGameList.remove(game)
        }
        fakeGameList.postValue(newGameList)
    }
}