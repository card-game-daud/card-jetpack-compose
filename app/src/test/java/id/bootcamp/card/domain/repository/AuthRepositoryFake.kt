package id.bootcamp.card.domain.repository

import id.bootcamp.card.domain.model.AnonymousUserModel

class AuthRepositoryFake: AuthRepository {
    private var currentUser: AnonymousUserModel? = null

    override fun getCurrentUser(): AnonymousUserModel? {
        return currentUser
    }

    override suspend fun signInAnonymously(): AnonymousUserModel? {
        currentUser = AnonymousUserModel("fake_id", "fake_name")
        return currentUser
    }

    override suspend fun updateUserName(newName: String): AnonymousUserModel? {
        currentUser?.name = newName
        return currentUser
    }
}