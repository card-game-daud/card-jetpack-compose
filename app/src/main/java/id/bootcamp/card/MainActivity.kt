package id.bootcamp.card

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.platform.LocalContext
import androidx.lifecycle.viewmodel.compose.viewModel
import com.google.firebase.Firebase
import com.google.firebase.appcheck.appCheck
import com.google.firebase.appcheck.playintegrity.PlayIntegrityAppCheckProviderFactory
import com.google.firebase.initialize
import id.bootcamp.card.ui.Navigation
import id.bootcamp.card.ui.theme.CardTheme
import id.bootcamp.card.ui.theme.ChosenTheme
import id.bootcamp.card.ui.theme.DarkTheme
import id.bootcamp.card.ui.theme.LocalTheme
import id.bootcamp.card.ui.viewmodel.MainViewModelFactory
import id.bootcamp.card.ui.viewmodel.ThemeViewModel

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Firebase.initialize(context = this)
        Firebase.appCheck.installAppCheckProviderFactory(
            PlayIntegrityAppCheckProviderFactory.getInstance(),
        )
        setContent {
            val themeViewModel: ThemeViewModel = viewModel(
                factory = MainViewModelFactory.getInstance(LocalContext.current)
            )
            val darkThemeSettings by themeViewModel.useDarkTheme.observeAsState()
            val isSystemInDarkTheme = isSystemInDarkTheme()
            val useDarkTheme = DarkTheme (
                when (darkThemeSettings) {
                    ChosenTheme.DARK -> true
                    ChosenTheme.LIGHT -> false
                    else -> isSystemInDarkTheme
                }
            )

            CompositionLocalProvider(LocalTheme.provides(useDarkTheme)) {
                CardTheme(darkTheme = LocalTheme.current.isDark) {
                    Navigation(
                        onChangeDarkTheme = { newSetting -> themeViewModel.changeTheme(newSetting) }
                    )
                }
            }
        }
    }
}