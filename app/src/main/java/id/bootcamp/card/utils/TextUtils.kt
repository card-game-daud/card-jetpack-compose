package id.bootcamp.card.utils

fun String.convertConstantCaseToTitleCase(): String {
    return this.split("_")
        .joinToString(" ") { word -> word.replaceFirstChar { it.lowercase()} }
}