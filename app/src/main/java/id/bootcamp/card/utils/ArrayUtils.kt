package id.bootcamp.card.utils

import id.bootcamp.card.domain.model.PlayerDeck
import id.bootcamp.card.domain.model.PlayerDiscardPile

fun Array<PlayerDeck>.setById(id: Int, value: PlayerDeck) {
    val updatedIndex = this.indexOf(this.find { it.id == id })
    this[updatedIndex] = value
}

fun Array<PlayerDeck>.getById(id: Int): PlayerDeck {
    val retrievedIndex = this.indexOf(this.find { it.id == id })
    return this[retrievedIndex]
}

fun Array<PlayerDiscardPile>.getById(id: Int): PlayerDiscardPile {
    val retrievedIndex = this.indexOf(this.find { it.id == id })
    return this[retrievedIndex]
}