package id.bootcamp.card.utils

import androidx.compose.runtime.snapshots.SnapshotStateList

fun <T> List<T>.toSnapshotStateList(): SnapshotStateList<T> {
    val snapshotStateList = SnapshotStateList<T>()
    snapshotStateList.addAll(this)
    return snapshotStateList
}