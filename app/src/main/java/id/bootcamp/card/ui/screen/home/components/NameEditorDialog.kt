package id.bootcamp.card.ui.screen.home.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import id.bootcamp.card.ui.theme.CardTheme

@Composable
fun NameEditorDialog(
    originalName: String,
    onDismissRequest: () -> Unit,
    onConfirmChange: (String) -> Unit
) {
    var name by rememberSaveable {
        mutableStateOf(originalName)
    }
    Dialog(onDismissRequest = onDismissRequest) {
        Card {
            Text(
                text = "Change Your Name",
                style = MaterialTheme.typography.titleLarge,
                modifier = Modifier
                    .padding(16.dp, 16.dp, 16.dp, 8.dp)
                    .align(Alignment.CenterHorizontally)
            )
            ChangeNameTextField(name = name, onNameEdited = { name = it })
            Row(
                modifier = Modifier
                    .padding(16.dp, 8.dp, 16.dp, 16.dp)
                    .align(Alignment.CenterHorizontally),
                horizontalArrangement = Arrangement.spacedBy(8.dp)
            ) {
                ConfirmNameChangeButton(
                    onClick = {
                        onConfirmChange(name)
                        onDismissRequest()
                    }
                )
                CancelNameChangeButton(onClick = onDismissRequest)
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ChangeNameTextField(
    name: String,
    onNameEdited: (String) -> Unit,
    modifier: Modifier = Modifier
) {
    OutlinedTextField(
        value = name,
        onValueChange = onNameEdited,
        singleLine = true,
        modifier = modifier.padding(16.dp, 8.dp)
    )
}

@Composable
fun ConfirmNameChangeButton(onClick: () -> Unit, modifier: Modifier = Modifier) {
    Button(onClick = onClick, modifier = modifier) {
        Text("Change")
    }
}

@Composable
fun CancelNameChangeButton(onClick: () -> Unit, modifier: Modifier = Modifier) {
    OutlinedButton(onClick = onClick, modifier = modifier) {
        Text("Cancel")
    }
}

@Preview
@Composable
fun NameEditorDialogPreview() {
    CardTheme {
        NameEditorDialog(originalName = "tes", onDismissRequest = { }, onConfirmChange = { })
    }
}