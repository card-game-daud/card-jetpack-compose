package id.bootcamp.card.ui.screen.play.components

import android.annotation.SuppressLint
import android.content.res.Configuration
import androidx.compose.animation.animateColor
import androidx.compose.animation.core.RepeatMode
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.rememberInfiniteTransition
import androidx.compose.animation.core.tween
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Divider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.unit.dp
import id.bootcamp.card.domain.model.CardModel
import id.bootcamp.card.domain.model.PlayerDeck
import id.bootcamp.card.domain.model.PlayerDiscardPile
import id.bootcamp.card.ui.screen.play.modifiers.responsivePlayerCardAreaDividerSize
import id.bootcamp.card.ui.theme.LocalTheme
import id.bootcamp.card.ui.theme.PlayScreenBackgroundDark
import id.bootcamp.card.ui.theme.PlayScreenBackgroundLight

@Composable
fun PlayArea(
    playerNames: Array<String>,
    playersDeck: Array<PlayerDeck>,
    playersDiscardPile: Array<PlayerDiscardPile>,
    drawPile: SnapshotStateList<CardModel>,
    currentPlayerId: Int,
    onDrawFromDiscardPile: (CardModel) -> Unit,
    onDrawFromDrawPile: (CardModel) -> Unit,
    onDiscardCard: (CardModel) -> Unit
) {
    Surface {
        Box(
            Modifier
                .fillMaxSize()
                .background(if (LocalTheme.current.isDark) PlayScreenBackgroundDark else PlayScreenBackgroundLight)
                .padding(8.dp)
        ) {
            PlayerArea(
                name = playerNames[0],
                listCard = playersDeck[0].listCard,
                topDiscardedCard = playersDiscardPile[0].listCard.lastOrNull(),
                isUser = true,
                isCurrentPlayer = currentPlayerId == 0,
                isVertical = false,
                modifier = Modifier.align(Alignment.BottomCenter),
                onDiscardCard = onDiscardCard,
                onDrawFromDiscardPile = onDrawFromDiscardPile
            )
            PlayerArea(
                name = playerNames[1],
                listCard = playersDeck[1].listCard,
                topDiscardedCard = playersDiscardPile[1].listCard.lastOrNull(),
                isUser = false,
                isCurrentPlayer = currentPlayerId == 1,
                isVertical = true,
                modifier = Modifier.align(Alignment.CenterStart),
                flipLayout = true
            )
            PlayerArea(
                name = playerNames[2],
                listCard = playersDeck[2].listCard,
                topDiscardedCard = playersDiscardPile[2].listCard.lastOrNull(),
                isUser = false,
                isCurrentPlayer = currentPlayerId == 2,
                isVertical = false,
                modifier = Modifier.align(Alignment.TopCenter),
                flipLayout = true
            )
            PlayerArea(
                name = playerNames[3],
                listCard = playersDeck[3].listCard,
                topDiscardedCard = playersDiscardPile[3].listCard.lastOrNull(),
                isUser = false,
                isCurrentPlayer = currentPlayerId == 3,
                isVertical = true,
                modifier = Modifier.align(Alignment.CenterEnd),
                flipLayout = false
            )
            if (drawPile.isNotEmpty()) {
                PlayingCard(
                    card = drawPile.last(),
                    isHidden = true,
                    modifier = Modifier.align(Alignment.Center),
                    onClick = onDrawFromDrawPile
                )
            }
        }
    }
}

@SuppressLint("SwitchIntDef")
@Composable
private fun PlayerArea(
    name: String,
    listCard: List<CardModel>,
    topDiscardedCard: CardModel?,
    isUser: Boolean,
    isCurrentPlayer: Boolean,
    isVertical: Boolean,
    modifier: Modifier = Modifier,
    flipLayout: Boolean = false,
    onDiscardCard: (CardModel) -> Unit = {},
    onDrawFromDiscardPile: (CardModel) -> Unit = {},
) {
    when (LocalConfiguration.current.orientation) {
        Configuration.ORIENTATION_PORTRAIT -> {
            Column(
                modifier = modifier,
                horizontalAlignment = if (!flipLayout)
                    Alignment.End
                else Alignment.Start,
                verticalArrangement = Arrangement.spacedBy(8.dp)
            ) {
                if (flipLayout) {
                    PlayerName(
                        name = name,
                        isCurrentPlayer = isCurrentPlayer
                    )
                }
                PlayerCardArea(
                    listCard = listCard,
                    topDiscardedCard = topDiscardedCard,
                    isUser = isUser,
                    isVertical = isVertical,
                    flipLayout = flipLayout,
                    verticalDivider = false,
                    onDiscardCard = onDiscardCard,
                    onDrawFromDiscardPile = onDrawFromDiscardPile
                )
                if (!flipLayout) {
                    PlayerName(
                        name = name,
                        isCurrentPlayer = isCurrentPlayer
                    )
                }
            }
        }
        Configuration.ORIENTATION_LANDSCAPE -> {
            Column(modifier = modifier) {
                if (!flipLayout) {
                    PlayerName(
                        name = name,
                        isCurrentPlayer = isCurrentPlayer,
                        modifier = Modifier.align(Alignment.End)
                    )
                }
                Row(
                    verticalAlignment = if (!flipLayout)
                        Alignment.Top
                    else Alignment.Bottom,
                    horizontalArrangement = Arrangement.spacedBy(8.dp)
                ) {
                    PlayerCardArea(
                        listCard = listCard,
                        topDiscardedCard = topDiscardedCard,
                        isUser = isUser,
                        isVertical = isVertical,
                        flipLayout = if (!isVertical) !flipLayout else flipLayout,
                        verticalDivider = true,
                        onDiscardCard = onDiscardCard,
                        onDrawFromDiscardPile = onDrawFromDiscardPile
                    )
                }
                if (flipLayout) {
                    PlayerName(
                        name = name,
                        isCurrentPlayer = isCurrentPlayer,
                        modifier = Modifier.align(Alignment.Start)
                    )
                }
            }
        }
    }
}

@Composable
fun PlayerName(
    name: String,
    isCurrentPlayer: Boolean,
    modifier: Modifier = Modifier
) {
    val nameColor = if (isCurrentPlayer) {
        val currentPlayerEffectTransition = rememberInfiniteTransition(label = "currentPlayerEffect")
        currentPlayerEffectTransition.animateColor(
            initialValue = MaterialTheme.colorScheme.onSurface,
            targetValue = MaterialTheme.colorScheme.primary,
            animationSpec = infiniteRepeatable(
                animation = tween(500),
                repeatMode = RepeatMode.Reverse
            ),
            label = "currentPlayerColor"
        ).value
    } else {
        MaterialTheme.colorScheme.onSurface
    }
    Text(
        text = name,
        modifier = modifier,
        color = nameColor
    )
}

@Composable
fun PlayerCardArea(
    listCard: List<CardModel>,
    topDiscardedCard: CardModel?,
    isUser: Boolean,
    isVertical: Boolean,
    flipLayout: Boolean,
    verticalDivider: Boolean,
    onDiscardCard: (CardModel) -> Unit = {},
    onDrawFromDiscardPile: (CardModel) -> Unit = {}
) {
    // Card's and name's orientation is the opposite of deck's orientation
    if (!flipLayout) {
        if (topDiscardedCard != null) {
            PlayingCard(
                card = topDiscardedCard,
                isHidden = false,
                isVertical = !isVertical,
                onClick = onDrawFromDiscardPile
            )
            // The vertical deck is bordered on the long side when the line is horizontal
            // The horizontal deck is bordered on the long side when the line is vertical
            PlayerCardAreaDivider(
                divideAtCardLongSide = isVertical.xor(verticalDivider),
                isVertical = verticalDivider
            )
        }
        PlayingCardDeck(
            listCard = listCard,
            isVisible = isUser,
            isVertical = isVertical,
            onClickCard = onDiscardCard
        )
    } else {
        PlayingCardDeck(
            listCard = listCard,
            isVisible = isUser,
            isVertical = isVertical,
            onClickCard = onDiscardCard
        )
        if (topDiscardedCard != null) {
            // The vertical deck is bordered on the long side when the line is horizontal
            // The horizontal deck is bordered on the long side when the line is vertical
            PlayerCardAreaDivider(
                divideAtCardLongSide = isVertical.xor(verticalDivider),
                isVertical = verticalDivider
            )
            PlayingCard(
                card = topDiscardedCard,
                isHidden = false,
                isVertical = !isVertical,
                onClick = onDrawFromDiscardPile
            )
        }
    }
}

@Composable
fun PlayerCardAreaDivider(
    divideAtCardLongSide: Boolean,
    isVertical: Boolean,
    modifier: Modifier = Modifier
) {
    val config = LocalConfiguration.current
    val screenWidth = config.screenWidthDp
    val screenHeight = config.screenHeightDp
    Divider(
        modifier = modifier.responsivePlayerCardAreaDividerSize(
            isVertical, divideAtCardLongSide, screenWidth, screenHeight
        ),
        color = MaterialTheme.colorScheme.onSurface
    )
}