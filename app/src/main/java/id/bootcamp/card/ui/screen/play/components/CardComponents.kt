package id.bootcamp.card.ui.screen.play.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import id.bootcamp.card.R
import id.bootcamp.card.domain.model.CardModel
import id.bootcamp.card.ui.screen.play.modifiers.responsivePlayingCardSize
import id.bootcamp.card.utils.convertConstantCaseToTitleCase

@Composable
fun PlayingCardDeck(listCard: List<CardModel>, isVisible: Boolean, isVertical: Boolean, modifier: Modifier = Modifier, onClickCard: (CardModel) -> Unit = {}) {
    when (isVertical) {
        true -> {
            Column(
                verticalArrangement = Arrangement.spacedBy(4.dp),
                modifier = modifier
            ) {
                PlayingCardDeckContent(listCard = listCard, isVisible = isVisible, isVertical = true, onClickCard = onClickCard)
            }
        }
        false -> {
            Row(
                horizontalArrangement = Arrangement.spacedBy(4.dp),
                modifier = modifier
            ) {
                PlayingCardDeckContent(listCard = listCard, isVisible = isVisible, isVertical = false, onClickCard = onClickCard)
            }
        }
    }
}

@Composable
fun PlayingCardDeckContent(listCard: List<CardModel>, isVisible: Boolean, isVertical: Boolean, onClickCard: (CardModel) -> Unit = {}) {
    for (card in listCard) {
        // Card's orientation is the opposite of deck's orientation
        PlayingCard(card = card, isHidden = !isVisible, isVertical = !isVertical, onClick = onClickCard)
    }
}

@Composable
fun PlayingCard(
    card: CardModel,
    isHidden: Boolean,
    modifier: Modifier = Modifier,
    isVertical: Boolean = true,
    onClick: (CardModel) -> Unit = {}
) {
    val config = LocalConfiguration.current
    val screenWidth = config.screenWidthDp
    val screenHeight = config.screenHeightDp
    Image(
        painter = painterResource(id = if (!isHidden)
            card.image
        else R.drawable.hidden_card),
        contentDescription = if (!isHidden)
            card.name.convertConstantCaseToTitleCase()
        else "Unknown Card",
        contentScale = ContentScale.Fit,
        modifier = modifier
            .responsivePlayingCardSize(isVertical, screenWidth, screenHeight)
            .clickable { onClick(card) }
    )
}