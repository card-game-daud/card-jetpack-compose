package id.bootcamp.card.ui.screen.play.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.window.Dialog
import id.bootcamp.card.ui.theme.CardTheme

// The "Save" action itself is done by only leaving without executing onDelete
@Composable
fun SaveOptionsDialog(
    onDelete: () -> Unit,
    onLeave: () -> Unit,
    onDismiss: () -> Unit
) {
    Dialog(onDismissRequest = onDismiss) {
        Card {
            Text(
                text = "Apakah Anda mau menyimpan permainan ini?",
                fontSize = 20.sp,
                textAlign = TextAlign.Justify,
                modifier = Modifier.padding(12.dp)
            )
            Text(
                text = "Jika tidak, permainan ini akan dihapus selamanya.",
                textAlign = TextAlign.Justify,
                modifier = Modifier.padding(12.dp, 4.dp)
            )
            Row(
                horizontalArrangement = Arrangement.spacedBy(8.dp),
                modifier = Modifier.padding(12.dp)
            ) {
                Button(onClick = {
                    onLeave()
                }) {
                    Text(text = "Ya")
                }
                OutlinedButton(onClick = {
                    onDelete()
                    onLeave()
                }) {
                    Text(text = "Tidak")
                }
            }
        }
    }
}

@Preview
@Composable
fun SaveOptionsDialogPreview() {
    CardTheme {
        SaveOptionsDialog(onLeave = { }, onDelete = { }, onDismiss = {})
    }
}