package id.bootcamp.card.ui.theme

import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color

val Purple80 = Color(0xFFD0BCFF)
val PurpleGrey80 = Color(0xFFCCC2DC)
val Pink80 = Color(0xFFEFB8C8)
val PlayGreenLight = Color(0xFF3DCC55)
val PlayScreenBackgroundLight = Brush.radialGradient(listOf(PlayGreenLight, PlayGreenLight.copy(alpha = 0.8F)))

val Purple40 = Color(0xFF6650a4)
val PurpleGrey40 = Color(0xFF625b71)
val Pink40 = Color(0xFF7D5260)
val PlayGreenDark = Color(0xFF046307)
val PlayScreenBackgroundDark = Brush.radialGradient(listOf(PlayGreenDark, PlayGreenDark.copy(alpha = 0.8F)))