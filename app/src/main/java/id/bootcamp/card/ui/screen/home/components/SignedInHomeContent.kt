package id.bootcamp.card.ui.screen.home.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import id.bootcamp.card.domain.model.AnonymousUserModel

@Composable
fun SignedInHomeContent(
    userData: AnonymousUserModel,
    enabled: Boolean,
    onChooseSinglePlayer: () -> Unit,
    onChooseMultiPlayer: () -> Unit,
    onChooseEditName: () -> Unit
) {
    Column(
        verticalArrangement = Arrangement.spacedBy(8.dp, Alignment.CenterVertically),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        HomeHeader()
        SinglePlayerButton(enabled = enabled, onClick = onChooseSinglePlayer )
        MultiPlayerButton(enabled = enabled, onClick = onChooseMultiPlayer)
        NameButton(
            currentName = userData.name,
            enabled = enabled,
            onClick = onChooseEditName,
            modifier = Modifier.padding(16.dp)
        )
    }
}

@Composable
fun HomeHeader(modifier: Modifier = Modifier) {
    Text(
        text = "Game Kartu 41",
        style = MaterialTheme.typography.titleLarge,
        fontSize = 40.sp,
        modifier = modifier.padding(8.dp, 8.dp, 8.dp, 16.dp)
    )
}

@Composable
fun SinglePlayerButton(enabled: Boolean, onClick: () -> Unit) {
    Button(enabled = enabled, onClick = onClick) {
        Text("Single Player")
    }
}

@Composable
fun MultiPlayerButton(enabled: Boolean, onClick: () -> Unit) {
    Button(enabled = enabled, onClick = onClick) {
        Text("Multi Player")
    }
}

@Composable
fun NameButton(currentName: String, enabled: Boolean, onClick: () -> Unit, modifier: Modifier = Modifier) {
    OutlinedButton(onClick = onClick, enabled = enabled, modifier = modifier) {
        Text(
            text = buildAnnotatedString {
                withStyle(SpanStyle(color = MaterialTheme.colorScheme.onSurface)) {
                    append("Name: ")
                }
                append(currentName)
            },
            style = MaterialTheme.typography.bodyLarge,
            fontSize = 20.sp,
            modifier = Modifier.padding(12.dp)
        )
    }
}