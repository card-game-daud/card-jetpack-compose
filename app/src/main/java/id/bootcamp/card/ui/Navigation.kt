package id.bootcamp.card.ui

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.platform.LocalContext
import androidx.lifecycle.viewmodel.compose.viewModel
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import id.bootcamp.card.ui.screen.home.HomeScreen
import id.bootcamp.card.ui.screen.lobby.LobbyScreen
import id.bootcamp.card.ui.screen.play.PlayScreen
import id.bootcamp.card.ui.theme.ChosenTheme
import id.bootcamp.card.ui.viewmodel.HomeViewModel
import id.bootcamp.card.ui.viewmodel.LobbyViewModel
import id.bootcamp.card.ui.viewmodel.MainViewModelFactory
import id.bootcamp.card.ui.viewmodel.MultiPlayerViewModel
import id.bootcamp.card.ui.viewmodel.OfflinePlayViewModel
import id.bootcamp.card.ui.viewmodel.OnlinePlayViewModel

@Composable
fun Navigation(onChangeDarkTheme: (ChosenTheme) -> Unit) {
    val navController = rememberNavController()
    NavHost(navController = navController, startDestination = "home") {
        composable("home") {
            val context = LocalContext.current
            val viewModel = viewModel(
                modelClass = HomeViewModel::class.java,
                factory = MainViewModelFactory.getInstance(context)
            )
            HomeScreen(
                viewModel = viewModel,
                onChooseOfflineSinglePlayer = {
                    navController.navigate("single_player")
                },
                onChooseOnlineSinglePlayer = {
                    navController.navigate("online_single_player")
                },
                onChooseMultiPlayer = {
                    navController.navigate("multi_player_lobby")
                },
                onHasOngoingGame = { gameId, userId ->
                    navController.navigate("multi_player/${gameId}/${userId}?rejoin=${true}")
                },
                onChangeDarkTheme = onChangeDarkTheme
            )
        }
        composable("single_player") {
            val context = LocalContext.current
            val viewModel = viewModel(
                modelClass = OfflinePlayViewModel::class.java,
                factory = MainViewModelFactory.getInstance(context)
            )
            PlayScreen(viewModel, onLeaveGame = { navController.popBackStack() })
        }
        composable("online_single_player") {
            val context = LocalContext.current
            val viewModel = viewModel(
                modelClass = OnlinePlayViewModel::class.java,
                factory = MainViewModelFactory.getInstance(context)
            )
            PlayScreen(viewModel, onLeaveGame = { navController.popBackStack() })
        }
        composable("multi_player_lobby") {
            val context = LocalContext.current
            val viewModel = viewModel(
                modelClass = LobbyViewModel::class.java,
                factory = MainViewModelFactory.getInstance(context)
            )
            LobbyScreen(
                viewModel = viewModel,
                onGameStart = { userId, gameData, rejoin ->
                    navController.navigate("multi_player/${gameData.id}/${userId}?rejoin=${rejoin}")
                },
                onBack = { navController.popBackStack() }
            )
        }
        composable("multi_player/{gameId}/{userId}?rejoin={rejoin}",
            arguments = listOf(
                navArgument("rejoin") { type = NavType.BoolType }
            )
        ) {
            val gameId = it.arguments?.getString("gameId")
            val userId = it.arguments?.getString("userId")
            val rejoin = it.arguments?.getBoolean("rejoin") ?: false

            val context = LocalContext.current
            val viewModel = viewModel(
                modelClass = MultiPlayerViewModel::class.java,
                factory = MainViewModelFactory.getInstance(context)
            )
            if (gameId != null && userId != null) {
                if (!viewModel.isSetupDone) {
                    LaunchedEffect(key1 = Unit) {
                        if (!rejoin) {
                            viewModel.getGameFromLobby(gameId)
                        } else {
                            viewModel.getGameFromOngoingGames(gameId)
                        }
                    }
                }
                val gameLiveData by viewModel.startingGameData.observeAsState()
                val gameDataFromLobby = gameLiveData
                if (gameDataFromLobby != null) {
                    PlayScreen(
                        viewModel = viewModel,
                        startingGameData = gameDataFromLobby,
                        userId = userId,
                        onLeaveGame = {
                            navController.popBackStack()
                        }
                    )
                }
            }
        }
    }
}