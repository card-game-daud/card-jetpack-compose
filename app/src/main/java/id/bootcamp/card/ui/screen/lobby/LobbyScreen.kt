package id.bootcamp.card.ui.screen.lobby

import androidx.activity.ComponentActivity
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import id.bootcamp.card.domain.model.LobbyGameModel
import id.bootcamp.card.ui.screen.lobby.components.CancelGameDialog
import id.bootcamp.card.ui.screen.lobby.components.CreateGameButton
import id.bootcamp.card.ui.shared.components.DisconnectedDialog
import id.bootcamp.card.ui.screen.lobby.components.LobbyGameList
import id.bootcamp.card.ui.screen.lobby.components.LobbyScaffold
import id.bootcamp.card.ui.theme.CardTheme
import id.bootcamp.card.ui.viewmodel.LobbyViewModel
import id.bootcamp.card.ui.viewmodel.MainViewModelFactory

@Composable
fun LobbyScreen(
    viewModel: LobbyViewModel,
    onGameStart: (String, LobbyGameModel, Boolean) -> Unit,
    onBack: () -> Unit
) {
    var showCancelGameDialog by rememberSaveable {
        mutableStateOf(false)
    }
    var showDisconnectedDialog by rememberSaveable {
        mutableStateOf(false)
    }
    var cancelledGameState: LobbyGameModel? by rememberSaveable {
        mutableStateOf(null)
    }
    val userLiveData by viewModel.userData.observeAsState()
    val lobbyLiveData by viewModel.lobbyGameData.observeAsState()
    val isConnected by viewModel.isConnected.observeAsState()

    // For smart cast purposes
    val userData = userLiveData
    val lobbyData = lobbyLiveData
    val cancelledGame = cancelledGameState

    // Should be only executed when isConnected value changes
    // Otherwise, it interferes with DisconnectedDialog's onClickBack
    LaunchedEffect(key1 = isConnected) {
        showDisconnectedDialog = (isConnected == false)
    }

    if (userData != null && lobbyData != null) {
        if (showCancelGameDialog && cancelledGame != null && cancelledGame.id in lobbyData.map { it.id }) {
            CancelGameDialog(
                isHost = cancelledGame.listPlayersId.lastOrNull() == userData.id,
                onDismiss = {
                    showCancelGameDialog = false
                    cancelledGameState = null
                },
                onConfirmCancel = { viewModel.leaveGame(userData.id, cancelledGame.id) }
            )
        }
        LobbyScaffold(currentUserName = userData.name) {
            LobbyGameList(
                userData = userData,
                lobbyData = lobbyData,
                showJoinButton = !viewModel.checkIsUserInGame(userData.id, lobbyData),
                onJoinGame = { game -> viewModel.joinGame(userData, game.id) },
                onStartGame = { game -> viewModel.startGame(game.id) },
                onCancelGame = { game ->
                    showCancelGameDialog = true
                    cancelledGameState = game
                },
                onGameStart = { game -> onGameStart(userData.id, game, false) },
                modifier = Modifier.fillMaxSize()
            )
            if (!viewModel.checkIsUserInGame(userData.id, lobbyData)) {
                CreateGameButton(
                    modifier = Modifier
                        .align(Alignment.BottomEnd)
                        .padding(16.dp),
                    onClick = { viewModel.addGame(userData) }
                )
            }
        }
    } else if (userData == null) {
        viewModel.enterLobby()
    }
    if (showDisconnectedDialog) {
        Surface(Modifier.fillMaxSize()) {
            DisconnectedDialog(
                canNavigateBack = true,
                onClickBack = {
                    showDisconnectedDialog = false
                    onBack()
                }
            )
        }
    }
}

@Preview
@Composable
fun LobbyScreenPreview() {
    val context = LocalContext.current
    val viewModel = viewModel(
        modelClass = LobbyViewModel::class.java,
        viewModelStoreOwner = LocalContext.current as ComponentActivity,
        factory = MainViewModelFactory.getInstance(context)
    )
    CardTheme {
        LobbyScreen(viewModel = viewModel, onGameStart = {_, _, _ -> }, onBack = {})
    }
}