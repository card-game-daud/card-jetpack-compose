package id.bootcamp.card.ui.shared.components

import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import id.bootcamp.card.R
import id.bootcamp.card.ui.theme.LocalTheme

@Composable
fun DarkThemeFloatingActionButton(
    onClick: () -> Unit,
    modifier: Modifier = Modifier
) {
    FloatingActionButton(
        onClick = onClick,
        modifier = modifier
    ) {
        val currentThemeIcon = if (LocalTheme.current.isDark) R.drawable.baseline_dark_mode_24 else R.drawable.baseline_light_mode_24
        val currentThemeName = if (LocalTheme.current.isDark) "Dark" else "Light"
        Icon(painter = painterResource(id = currentThemeIcon), contentDescription = "The current theme is $currentThemeName. Click to change it.")
    }
}