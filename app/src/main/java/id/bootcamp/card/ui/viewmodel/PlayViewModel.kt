package id.bootcamp.card.ui.viewmodel

import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.lifecycle.MutableLiveData
import id.bootcamp.card.domain.model.CardModel
import id.bootcamp.card.domain.model.DrawSource
import id.bootcamp.card.domain.model.GameStatus
import id.bootcamp.card.domain.model.LobbyGameModel
import id.bootcamp.card.domain.model.PlayerDeck
import id.bootcamp.card.domain.model.PlayerDiscardPile
import id.bootcamp.card.domain.model.PlayerModel
import id.bootcamp.card.domain.model.TurnStatus
import id.bootcamp.card.domain.usecase.CalculateDeckScoreUseCase

interface PlayViewModel {
    val drawPile: MutableLiveData<SnapshotStateList<CardModel>>
    val playersDeck: MutableLiveData<Array<PlayerDeck>>
    val playersDiscardPile: MutableLiveData<Array<PlayerDiscardPile>>
    val turnStatus: MutableLiveData<TurnStatus>
    val gameStatus: MutableLiveData<GameStatus>
    val playersScore: MutableLiveData<Array<Int>>
    val startingPlayer: MutableLiveData<Int>
    val players: MutableLiveData<Array<PlayerModel>>
    val currentPlayerId: MutableLiveData<Int>
    val isConnected: MutableLiveData<Boolean>

    val numPlayers: Int
    val numCardsPerDeck: Int
    val userId: Int

    suspend fun setupGame(startingGameData: LobbyGameModel?, userKey: String) {
        // First case is for multi-player; second case is for single-player
        if (startingGameData != null && startingGameData.listPlayersId.isNotEmpty()) {
            setPlayers(userKey, startingGameData)
            if (startingGameData.listPlayersId.last() == userKey && !startingGameData.rejoin) {
                startGame(startingGameData.id)
            } else {
                syncGameData(startingGameData.id)
            }
        } else {
            startGame()
        }
    }

    suspend fun startGame(gameId: String = "")

    fun syncGameData(gameId: String = "") {}

    fun setPlayers(userKey: String, gameData: LobbyGameModel) {}

    fun updatePlayerDeck(
        playerDeck: PlayerDeck,
        modification: SnapshotStateList<CardModel>.() -> Unit
    ): PlayerDeck {
        val playerId = playerDeck.id
        val updatedPlayerCardList = playerDeck.listCard
        updatedPlayerCardList.modification()
        return PlayerDeck(playerId, updatedPlayerCardList)
    }

    fun updatePlayersDiscardPile(
        modifiedPlayerId: Int,
        modification: PlayerDiscardPile.() -> Unit
    ): Array<PlayerDiscardPile> {
        val updatedPlayersDiscardPile = playersDiscardPile.value!!
        val updatedPlayerDiscardPile = updatedPlayersDiscardPile[modifiedPlayerId]
        updatedPlayerDiscardPile.modification()
        updatedPlayersDiscardPile[modifiedPlayerId] = updatedPlayerDiscardPile
        return updatedPlayersDiscardPile
    }

    // Returns the updated players' decks after a card is drawn
    fun drawCard(playerDeck: PlayerDeck, drawnCard: CardModel): Array<PlayerDeck>

    fun drawCard(playerDeck: PlayerDeck, drawSource: DrawSource, onFinish: () -> Unit = {})

    fun drawFromDrawPile(playerDeck: PlayerDeck, drawnCard: CardModel, onDrawFinish: () -> Unit = {})

    fun drawFromDiscardPile(playerDeck: PlayerDeck, drawnCard: CardModel, onDrawFinish: () -> Unit = {})

    fun discardCard(playerDeck: PlayerDeck, discardedCard: CardModel)

    fun computerMove(id: Int)

    fun checkShouldGameEnd(currentPlayerDeck: PlayerDeck): Boolean {
        if (CalculateDeckScoreUseCase()(currentPlayerDeck.listCard) == 41) {
            return true
        }
        if (drawPile.value!!.isEmpty()) {
            return true
        }
        return false
    }

    fun endGame()

    fun calculateAllPlayersScore() {
        val allPlayersScore = Array(4){0}
        for ((i, deck) in playersDeck.value!!.withIndex()) {
            allPlayersScore[i] = CalculateDeckScoreUseCase()(deck.listCard)
        }
        playersScore.postValue(allPlayersScore)
    }

    fun restartGame()
}