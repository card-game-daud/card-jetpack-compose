package id.bootcamp.card.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.bootcamp.card.data.datastore.MainDataStore
import id.bootcamp.card.domain.model.AnonymousUserModel
import id.bootcamp.card.domain.usecase.ChangeUserNameUseCase
import id.bootcamp.card.domain.usecase.GetAnonymousUserUseCase
import id.bootcamp.card.domain.usecase.GetCurrentGameUseCase
import id.bootcamp.card.ui.theme.ChosenTheme
import kotlinx.coroutines.launch

class HomeViewModel(
    private val dataStore: MainDataStore,
    private val getAnonymousUserUseCase: GetAnonymousUserUseCase,
    private val getCurrentGameUseCase: GetCurrentGameUseCase,
    private val changeUserNameUseCase: ChangeUserNameUseCase
): ViewModel() {
    val isDarkTheme = MutableLiveData<ChosenTheme>()
    val userData = MutableLiveData<AnonymousUserModel>()
    val isLoading = MutableLiveData<Boolean>()
    val errorToastMessage = MutableLiveData<String>()

    init {
        viewModelScope.launch {
            dataStore.loadTheme().collect{
                isDarkTheme.postValue(it ?: ChosenTheme.SYSTEM)
            }
        }
    }

    fun signIn(onHasOngoingGame: (String, String) -> Unit) = viewModelScope.launch {
        val user = getAnonymousUserUseCase()
        userData.postValue(user)
        if (user != null) {
            val currentGame = getCurrentGameUseCase(user.id)
            if (currentGame != null && currentGame.id.startsWith("-")) {
                onHasOngoingGame(currentGame.id, user.id)
            }
        }
    }

    fun changeUserName(newName: String) = viewModelScope.launch {
        isLoading.postValue(true)
        val updatedUserData = changeUserNameUseCase(newName)
        // Update the user data with the new name
        if (updatedUserData != null) {
            userData.postValue(updatedUserData)
        } else {
            errorToastMessage.postValue("Penggantian nama tidak berhasil")
        }
        isLoading.postValue(false)
    }
}