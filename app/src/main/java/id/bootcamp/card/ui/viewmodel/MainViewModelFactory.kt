package id.bootcamp.card.ui.viewmodel

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import id.bootcamp.card.data.datastore.MainDataStore
import id.bootcamp.card.data.repository.OnlinePlayRepositoryImpl
import id.bootcamp.card.data.repository.LobbyRepositoryImpl
import id.bootcamp.card.di.Injection
import id.bootcamp.card.domain.usecase.ChangeUserNameUseCase
import id.bootcamp.card.domain.usecase.ComputerMoveUseCase
import id.bootcamp.card.domain.usecase.EndOnlineGameUseCase
import id.bootcamp.card.domain.usecase.EnterLobbyUseCase
import id.bootcamp.card.domain.usecase.EnterMultiPlayerGameUseCase
import id.bootcamp.card.domain.usecase.GenerateDeckUseCase
import id.bootcamp.card.domain.usecase.GeneratePlayersArrayUseCase
import id.bootcamp.card.domain.usecase.GetAnonymousUserUseCase
import id.bootcamp.card.domain.usecase.GetCurrentGameUseCase
import id.bootcamp.card.domain.usecase.StartMultiPlayerGameUseCase
import id.bootcamp.card.domain.usecase.StartOnlineGameUseCase

class MainViewModelFactory(
    private val onlinePlayRepository: OnlinePlayRepositoryImpl,
    private val lobbyRepository: LobbyRepositoryImpl,
    private val dataStore: MainDataStore,
    private val generateDeckUseCase: GenerateDeckUseCase,
    private val computerMoveUseCase: ComputerMoveUseCase,
    private val getAnonymousUserUseCase: GetAnonymousUserUseCase,
    private val changeUserNameUseCase: ChangeUserNameUseCase,
    private val enterLobbyUseCase: EnterLobbyUseCase,
    private val startMultiPlayerGameUseCase: StartMultiPlayerGameUseCase,
    private val enterMultiPlayerGameUseCase: EnterMultiPlayerGameUseCase,
    private val generatePlayersArrayUseCase: GeneratePlayersArrayUseCase,
    private val startOnlineGameUseCase: StartOnlineGameUseCase,
    private val endOnlineGameUseCase: EndOnlineGameUseCase,
    private val getCurrentGameUseCase: GetCurrentGameUseCase
) : ViewModelProvider.Factory {

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(OnlinePlayViewModel::class.java)) {
            return OnlinePlayViewModel(
                onlinePlayRepository,
                getAnonymousUserUseCase,
                generatePlayersArrayUseCase,
                startOnlineGameUseCase,
                endOnlineGameUseCase,
                computerMoveUseCase
            ) as T
        } else if (modelClass.isAssignableFrom(OfflinePlayViewModel::class.java)) {
            return OfflinePlayViewModel(
                getAnonymousUserUseCase,
                generatePlayersArrayUseCase,
                generateDeckUseCase,
                computerMoveUseCase
            ) as T
        } else if (modelClass.isAssignableFrom(LobbyViewModel::class.java)) {
            return LobbyViewModel(lobbyRepository, enterLobbyUseCase) as T
        } else if (modelClass.isAssignableFrom(MultiPlayerViewModel::class.java)) {
            return MultiPlayerViewModel(
                onlinePlayRepository,
                lobbyRepository,
                startMultiPlayerGameUseCase,
                enterMultiPlayerGameUseCase,
                generatePlayersArrayUseCase,
                endOnlineGameUseCase,
                computerMoveUseCase
            ) as T
        } else if (modelClass.isAssignableFrom(HomeViewModel::class.java)) {
            return HomeViewModel(
                dataStore,
                getAnonymousUserUseCase,
                getCurrentGameUseCase,
                changeUserNameUseCase
            ) as T
        } else if (modelClass.isAssignableFrom(ThemeViewModel::class.java)) {
            return ThemeViewModel(dataStore) as T
        }
        return super.create(modelClass)
    }

    companion object {
        @Volatile
        private var instance: MainViewModelFactory? = null
        fun getInstance(context: Context): MainViewModelFactory =
            instance ?: synchronized(this) {
                instance ?: MainViewModelFactory(
                    OnlinePlayRepositoryImpl.getInstance(),
                    LobbyRepositoryImpl.getInstance(),
                    MainDataStore(context),
                    Injection.provideGenerateDeckUseCase(),
                    Injection.provideComputerMoveUseCase(),
                    Injection.provideGetAnonymousUserUseCase(),
                    Injection.provideChangeUserNameUseCase(),
                    Injection.provideEnterLobbyUseCase(),
                    Injection.provideStartMultiPlayerGameUseCase(),
                    Injection.provideEnterMultiPlayerGameUseCase(),
                    Injection.provideGeneratePlayersArrayUseCase(),
                    Injection.provideStartOnlineGameUseCase(),
                    Injection.provideEndOnlineGameUseCase(),
                    Injection.provideGetCurrentGameUseCase()
                )
            }.also { instance = it }
    }
}