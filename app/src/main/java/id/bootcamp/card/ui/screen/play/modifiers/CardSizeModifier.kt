package id.bootcamp.card.ui.screen.play.modifiers

import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.material3.DividerDefaults
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.draw.scale
import androidx.compose.ui.unit.dp

// Calculate playing card's size in case the screen is too small
// Height here means the long side, even when it isn't the vertical one
fun calcResponsivePlayingCardSize(
    screenWidthDp: Int,
    screenHeightDp: Int
): Pair<Int, Int> {
    val isLandscape = screenWidthDp > screenHeightDp
    val (defaultScreenWidth, defaultScreenHeight) = if (!isLandscape) arrayOf(480, 900) else arrayOf(900, 480)
    val (defaultCardWidth, defaultCardHeight) = arrayOf(60, 90)
    val cardWidth = if (screenWidthDp < defaultScreenWidth) {
        screenWidthDp * defaultCardWidth / defaultScreenWidth
    } else defaultCardWidth
    val cardHeight = if (screenHeightDp < defaultScreenHeight) {
        screenHeightDp * defaultCardHeight / defaultScreenHeight
    } else defaultCardHeight
    return cardWidth to cardHeight
}

fun Modifier.responsivePlayingCardSize(
    isVertical: Boolean,
    screenWidthDp: Int,
    screenHeightDp: Int
): Modifier {
    val (cardWidth, cardHeight) = calcResponsivePlayingCardSize(screenWidthDp, screenHeightDp)
    return (if (!isVertical)
        this
            .size(cardHeight.dp, cardWidth.dp)
            .rotate(90F)
            .scale(1.5F) // rotate reduce the image's size for some reason, so this is the fix
    else this.size(cardWidth.dp, cardHeight.dp))
}

fun Modifier.responsivePlayerCardAreaDividerSize(
    isVertical: Boolean,
    divideAtCardLongSide: Boolean,
    screenWidthDp: Int,
    screenHeightDp: Int
): Modifier {
    val (cardWidth, cardHeight) = calcResponsivePlayingCardSize(screenWidthDp, screenHeightDp)
    return if (isVertical)
        this
            .height(if (divideAtCardLongSide) cardHeight.dp else cardWidth.dp)
            .width(DividerDefaults.Thickness)
    else
        this
            .width(if (divideAtCardLongSide) cardHeight.dp else cardWidth.dp)
            .height(DividerDefaults.Thickness)
}