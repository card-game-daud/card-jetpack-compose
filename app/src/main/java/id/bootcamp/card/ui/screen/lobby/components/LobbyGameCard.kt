package id.bootcamp.card.ui.screen.lobby.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Card
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import id.bootcamp.card.domain.model.AnonymousUserModel
import id.bootcamp.card.domain.model.LobbyGameModel

@Composable
fun LobbyGameCard(
    game: LobbyGameModel,
    userId: String,
    showJoinButton: Boolean,
    onClickJoin: () -> Unit,
    onClickStart: () -> Unit,
    onClickCancel: () -> Unit,
    modifier: Modifier = Modifier
) {
    Card(modifier) {
        Text(
            text = "Game ID: ${game.id}",
            style = MaterialTheme.typography.titleMedium,
            modifier = Modifier.padding(12.dp, 12.dp, 12.dp, 0.dp)
        )
        Text(
            text = buildAnnotatedString {
                append("Players:")
                appendPlayerNameBullet(game.listPlayers.last())
                withStyle(SpanStyle(fontStyle = FontStyle.Italic)) {
                    append(" (host)")
                }
                val nonHostPlayers = game.listPlayers.dropLast(1)
                nonHostPlayers.forEach { player ->
                    appendPlayerNameBullet(player)
                }
            },
            modifier = Modifier.padding(16.dp, 8.dp, 16.dp, 12.dp)
        )
        Row(
            modifier = Modifier.padding(16.dp, 0.dp, 16.dp, 16.dp),
            horizontalArrangement = Arrangement.spacedBy(8.dp)
        ) {
            // The "Join" and "Start" buttons are mutually-exclusive
            if (showJoinButton) {
                LobbyButton(text = "Join", onClick = onClickJoin)
            }
            if (game.listPlayersId.lastOrNull() == userId) {
                LobbyButton(text = "Start", onClick = onClickStart)
            }
            if (userId in game.listPlayersId) {
                LobbyButton(text = "Cancel", onClick = onClickCancel, useWarningColor = true)
            }
        }
    }
}

fun AnnotatedString.Builder.appendPlayerNameBullet(player: AnonymousUserModel) {
    append("\n")
    append(Typography.bullet)
    append("\t\t")
    append(player.name)
}