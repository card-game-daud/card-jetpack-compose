package id.bootcamp.card.ui.screen.home.components

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.size
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@Composable
fun LoadingIndicator(modifier: Modifier = Modifier) {
    Box {
        CircularProgressIndicator(
            modifier = modifier
                .size(48.dp)
                .align(Alignment.Center),
            color = MaterialTheme.colorScheme.inversePrimary
        )
    }
}