package id.bootcamp.card.ui.screen.lobby.components

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import id.bootcamp.card.domain.model.AnonymousUserModel
import id.bootcamp.card.domain.model.LobbyGameModel

@Composable
fun LobbyGameList(
    userData: AnonymousUserModel,
    lobbyData: List<LobbyGameModel>,
    showJoinButton: Boolean,
    onJoinGame: (LobbyGameModel) -> Unit,
    onStartGame: (LobbyGameModel) -> Unit,
    onCancelGame: (LobbyGameModel) -> Unit,
    onGameStart: (LobbyGameModel) -> Unit,
    modifier: Modifier = Modifier
) {
    LazyColumn(modifier) {
        item {
            Text(
                text = "Available Game(s)",
                style = MaterialTheme.typography.titleLarge,
                modifier = Modifier.padding(12.dp)
            )
        }
        items(lobbyData) { game ->
            if (game.listPlayers.isNotEmpty()) { // Precaution against NoSuchElementException
                LobbyGameCard(
                    game = game,
                    userId = userData.id,
                    showJoinButton = showJoinButton,
                    onClickJoin = { onJoinGame(game) },
                    onClickStart = { onStartGame(game) },
                    onClickCancel = { onCancelGame(game) },
                    modifier = Modifier
                        .padding(8.dp)
                        .fillMaxWidth()
                )
            }
            if (!game.isWaiting && userData.id in game.listPlayersId) {
                LaunchedEffect(key1 = Unit) {
                    onGameStart(game)
                }
            }
        }
        if (lobbyData.isEmpty()) {
            item {
                NoAvailableGameText(Modifier.padding(12.dp).fillMaxWidth())
            }
        }
    }
}

@Composable
fun NoAvailableGameText(modifier: Modifier = Modifier) {
    Text(
        text = "There is no available game to join right now.\nYou can create one using the button below or wait for other people to create one.",
        fontStyle = FontStyle.Italic,
        textAlign = TextAlign.Center,
        modifier = modifier
    )
}