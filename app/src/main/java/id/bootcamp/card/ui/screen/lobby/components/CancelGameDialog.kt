package id.bootcamp.card.ui.screen.lobby.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Card
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog

@Composable
fun CancelGameDialog(
    isHost: Boolean,
    onDismiss: () -> Unit,
    onConfirmCancel: () -> Unit
) {
    Dialog(
        onDismissRequest = onDismiss
    ) {
        Card {
            Text(
                text = if (isHost) "Cancel Game" else "Cancel Join",
                style = MaterialTheme.typography.titleLarge,
                modifier = Modifier.padding(16.dp)
            )
            Text(
                text = if (isHost)
                    "Are you sure you want to cancel the game?"
                else
                    "Are you sure you no longer want to join the game?",
                modifier = Modifier.padding(16.dp, 8.dp)
            )
            Row(
                horizontalArrangement = Arrangement.spacedBy(8.dp, Alignment.End),
                modifier = Modifier
                    .padding(16.dp)
                    .fillMaxWidth()
            ) {
                LobbyButton(
                    text = "Yes",
                    onClick = {
                        onConfirmCancel()
                        onDismiss()
                    },
                    useWarningColor = true
                )
                LobbyButton(
                    text = "No",
                    onClick = onDismiss
                )
            }
        }
    }
}