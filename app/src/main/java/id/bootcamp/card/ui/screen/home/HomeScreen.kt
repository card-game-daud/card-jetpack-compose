package id.bootcamp.card.ui.screen.home

import android.widget.Toast
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.lifecycle.viewmodel.compose.viewModel
import id.bootcamp.card.ui.screen.home.components.FailedSignInDisplay
import id.bootcamp.card.ui.screen.home.components.LoadingIndicator
import id.bootcamp.card.ui.screen.home.components.NameEditorDialog
import id.bootcamp.card.ui.screen.home.components.SignedInHomeContent
import id.bootcamp.card.ui.screen.home.components.SinglePlayerChoicesDialog
import id.bootcamp.card.ui.shared.components.DarkThemeDialog
import id.bootcamp.card.ui.shared.components.DarkThemeFloatingActionButton
import id.bootcamp.card.ui.theme.CardTheme
import id.bootcamp.card.ui.theme.ChosenTheme
import id.bootcamp.card.ui.viewmodel.HomeViewModel
import id.bootcamp.card.ui.viewmodel.MainViewModelFactory

@Composable
fun HomeScreen(
    viewModel: HomeViewModel,
    onChooseOfflineSinglePlayer: () -> Unit,
    onChooseOnlineSinglePlayer: () -> Unit,
    onChooseMultiPlayer: () -> Unit,
    onHasOngoingGame: (String, String) -> Unit,
    onChangeDarkTheme: (ChosenTheme) -> Unit
) {
    var showSinglePlayerDialog by rememberSaveable {
        mutableStateOf(false)
    }
    var showChangeNameDialog by rememberSaveable {
        mutableStateOf(false)
    }
    var showDarkThemeDialog by rememberSaveable {
        mutableStateOf(false)
    }
    val currentThemeLiveData by viewModel.isDarkTheme.observeAsState()
    val userLiveData by viewModel.userData.observeAsState()
    val isLoading by viewModel.isLoading.observeAsState(initial = false)
    val errorToastMessageLiveData by viewModel.errorToastMessage.observeAsState()

    // For smart cast purpose
    val currentTheme = currentThemeLiveData
    val userData = userLiveData
    val errorToastMessage = errorToastMessageLiveData

    if (showSinglePlayerDialog) {
        SinglePlayerChoicesDialog(
            onDismissRequest = { showSinglePlayerDialog = false },
            onChooseOffline = onChooseOfflineSinglePlayer,
            onChooseOnline = onChooseOnlineSinglePlayer
        )
    }
    if (showChangeNameDialog && userData != null) {
        NameEditorDialog(
            originalName = userData.name,
            onDismissRequest = { showChangeNameDialog = false },
            onConfirmChange = { if (it != userData.name) viewModel.changeUserName(it) }
        )
    }
    if (showDarkThemeDialog && currentTheme != null) {
        DarkThemeDialog(
            currentTheme = currentTheme,
            onDismissRequest = { showDarkThemeDialog = false },
            onConfirmChange = { newTheme -> onChangeDarkTheme(newTheme) }
        )
    }
    if (errorToastMessage != null) {
        val context = LocalContext.current
        LaunchedEffect(key1 = errorToastMessage) {
            Toast.makeText(context, errorToastMessage, Toast.LENGTH_SHORT).show()
            viewModel.errorToastMessage.postValue(null)
        }
    }
    // A surface container using the 'background' color from the theme
    Surface(
        modifier = Modifier.fillMaxSize(),
        color = MaterialTheme.colorScheme.background
    ) {
        if (isLoading) {
            LoadingIndicator()
        }
        if (userData != null) {
            SignedInHomeContent(
                userData = userData,
                enabled = !isLoading,
                onChooseSinglePlayer = { showSinglePlayerDialog = true },
                onChooseMultiPlayer = onChooseMultiPlayer,
                onChooseEditName = { showChangeNameDialog = true }
            )
        } else {
            // Executed here so that it won't be done again if the userId is known
            LaunchedEffect(key1 = Unit) {
                viewModel.signIn(onHasOngoingGame)
            }
            FailedSignInDisplay(onClickRetry = { viewModel.signIn(onHasOngoingGame) })
        }
        Box(Modifier.fillMaxSize()) {
            DarkThemeFloatingActionButton(
                onClick = { showDarkThemeDialog = true },
                modifier = Modifier
                    .align(Alignment.BottomEnd)
                    .padding(16.dp)
            )
        }
    }
}

@Preview(showBackground = true)
@Composable
fun HomeScreenPreview() {
    val context = LocalContext.current
    val viewModel = viewModel(
        modelClass = HomeViewModel::class.java,
        factory = MainViewModelFactory.getInstance(context)
    )
    CardTheme {
        HomeScreen(
            viewModel = viewModel,
            onChooseOfflineSinglePlayer = {},
            onChooseOnlineSinglePlayer = {},
            onChooseMultiPlayer = {},
            onHasOngoingGame = {_, _ -> },
            onChangeDarkTheme = {}
        )
    }
}