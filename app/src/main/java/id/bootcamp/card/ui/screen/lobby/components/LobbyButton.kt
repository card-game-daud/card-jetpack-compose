package id.bootcamp.card.ui.screen.lobby.components

import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier

@Composable
fun LobbyButton(
    text: String,
    onClick: () -> Unit,
    modifier: Modifier = Modifier,
    useWarningColor: Boolean = false
) {
    Button(
        modifier = modifier,
        onClick = onClick,
        colors = if (!useWarningColor) {
            ButtonDefaults.buttonColors()
        } else {
            ButtonDefaults.buttonColors(
                containerColor = MaterialTheme.colorScheme.errorContainer,
                contentColor = MaterialTheme.colorScheme.onErrorContainer
            )
        }
    ) {
        Text(text = text)
    }
}