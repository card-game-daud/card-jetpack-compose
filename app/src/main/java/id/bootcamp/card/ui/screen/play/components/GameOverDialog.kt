package id.bootcamp.card.ui.screen.play.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Card
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import id.bootcamp.card.domain.model.CardModel
import id.bootcamp.card.domain.model.PlayerDeck
import id.bootcamp.card.ui.theme.CardTheme

@Composable
fun GameOverDialog(
    playerNames: Array<String>,
    playersDeck: Array<PlayerDeck>,
    playersScore: Array<Int>,
    isRestartable: Boolean,
    onClickRestart: () -> Unit,
    onClickExit: () -> Unit
) {
    Dialog(
        onDismissRequest = onClickRestart,
        properties = DialogProperties(dismissOnBackPress = false, dismissOnClickOutside = false)
    ) {
        Card(modifier = Modifier.padding(8.dp)) {
            Text(
                text = "Game Over",
                style = MaterialTheme.typography.titleLarge,
                modifier = Modifier
                    .padding(12.dp)
                    .align(Alignment.CenterHorizontally)
            )
            LazyColumn(modifier = Modifier.weight(1.0F, fill = false)) {
                items(4) { i ->
                    PlayerScore(
                        playerName = playerNames[i],
                        playerScore = playersScore[i],
                        playerCards = playersDeck[i].listCard,
                        modifier = Modifier.padding(24.dp, 8.dp)
                    )
                }
            }
            Text(
                text = buildAnnotatedString {
                    withStyle(SpanStyle(fontWeight = FontWeight.Bold)) {
                        append("Winner:")
                    }
                    append(" ")
                    append(playerNames[playersScore.indexOf(playersScore.max())])
                },
                style = MaterialTheme.typography.bodyLarge,
                modifier = Modifier
                    .padding(16.dp, 8.dp)
                    .align(Alignment.CenterHorizontally)
            )
            GameOverDialogButtonRow(
                isRestartable = isRestartable,
                onClickRestart = onClickRestart,
                onClickExit = onClickExit,
                modifier = Modifier.align(Alignment.CenterHorizontally).padding(12.dp)
            )
        }
    }
}

@Composable
fun PlayerScore(
    playerName: String,
    playerScore: Int,
    playerCards: List<CardModel>,
    modifier: Modifier = Modifier
) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.spacedBy(8.dp),
        modifier = modifier
    ) {
        Text(
            text = buildAnnotatedString {
                withStyle(SpanStyle(fontWeight = FontWeight.Bold)) {
                    append(playerName)
                    append(":")
                }
                append(" ")
                append(playerScore.toString())
            },
            style = MaterialTheme.typography.bodyLarge
        )
        PlayingCardDeck(
            listCard = playerCards,
            isVisible = true,
            isVertical = false
        )
    }
}

@Composable
fun GameOverDialogButtonRow(
    isRestartable: Boolean,
    onClickRestart: () -> Unit,
    onClickExit: () -> Unit,
    modifier: Modifier = Modifier
) {
    Row(
        horizontalArrangement = Arrangement.spacedBy(16.dp, Alignment.CenterHorizontally),
        modifier = modifier
    ) {
        if (isRestartable) {
            Button(onClick = onClickRestart) {
                Text("Restart")
            }
        }
        Button(
            onClick = onClickExit,
            colors = ButtonDefaults.buttonColors(
                containerColor = MaterialTheme.colorScheme.errorContainer,
                contentColor = MaterialTheme.colorScheme.onErrorContainer
            )
        ) {
            Text(text = "Exit")
        }
    }
}

@Preview
@Composable
fun GameOverDialogPreview() {
    val dummyDeck = List(4) { CardModel.ACE_OF_CLUBS }
    CardTheme {
        GameOverDialog(
            playerNames = Array(4) { "tes" },
            playersDeck = Array(4) { PlayerDeck(it, dummyDeck) },
            playersScore = Array(4) { 44 },
            isRestartable = true,
            onClickRestart = { },
            onClickExit = { }
        )
    }
}
