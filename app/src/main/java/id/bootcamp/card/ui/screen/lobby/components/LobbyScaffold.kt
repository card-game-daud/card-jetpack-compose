package id.bootcamp.card.ui.screen.lobby.components

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.BoxScope
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun LobbyScaffold(
    currentUserName: String,
    content: @Composable (BoxScope.() -> Unit)
) {
    Scaffold(
        topBar = {
            LobbyTopBar(currentUserName)
        },
        containerColor = MaterialTheme.colorScheme.surface
    ) { bodyPadding ->
        Box(
            modifier = Modifier.padding(bodyPadding),
            content = content
        )
    }
}