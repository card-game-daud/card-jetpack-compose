package id.bootcamp.card.ui.viewmodel

import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import id.bootcamp.card.domain.mapper.rotateFromRemote
import id.bootcamp.card.domain.mapper.rotateToRemote
import id.bootcamp.card.domain.model.CardModel
import id.bootcamp.card.domain.model.DrawSource
import id.bootcamp.card.domain.model.GameStatus
import id.bootcamp.card.domain.model.LobbyGameModel
import id.bootcamp.card.domain.model.PlayGameModel
import id.bootcamp.card.domain.model.PlayerDeck
import id.bootcamp.card.domain.model.PlayerDiscardPile
import id.bootcamp.card.domain.model.PlayerModel
import id.bootcamp.card.domain.model.TurnStatus
import id.bootcamp.card.domain.repository.OnlinePlayRepository
import id.bootcamp.card.domain.repository.LobbyRepository
import id.bootcamp.card.domain.usecase.ComputerMoveUseCase
import id.bootcamp.card.domain.usecase.EndOnlineGameUseCase
import id.bootcamp.card.domain.usecase.EnterMultiPlayerGameUseCase
import id.bootcamp.card.domain.usecase.GeneratePlayersArrayUseCase
import id.bootcamp.card.domain.usecase.StartMultiPlayerGameUseCase
import id.bootcamp.card.utils.getById
import id.bootcamp.card.utils.setById
import java.util.Timer
import kotlin.concurrent.schedule
import kotlin.concurrent.timerTask

class MultiPlayerViewModel(
    private val onlinePlayRepository: OnlinePlayRepository,
    private val lobbyRepository: LobbyRepository,
    private val startMultiPlayerGameUseCase: StartMultiPlayerGameUseCase,
    private val enterMultiPlayerGameUseCase: EnterMultiPlayerGameUseCase,
    private val generatePlayersArrayUseCase: GeneratePlayersArrayUseCase,
    private val endOnlineGameUseCase: EndOnlineGameUseCase,
    private val computerMoveUseCase: ComputerMoveUseCase
): ViewModel(), PlayViewModel {
    val startingGameData = MutableLiveData<LobbyGameModel>()
    override val drawPile = MutableLiveData<SnapshotStateList<CardModel>>()
    override val playersDeck = MutableLiveData<Array<PlayerDeck>>()
    override val playersDiscardPile = MutableLiveData<Array<PlayerDiscardPile>>()
    private val playersTurnStatus = MutableLiveData<Array<TurnStatus>>()
    override val turnStatus = MutableLiveData<TurnStatus>()
    override val gameStatus = MutableLiveData<GameStatus>(GameStatus.STARTING)
    override val playersScore = MutableLiveData<Array<Int>>()
    override val startingPlayer = MutableLiveData<Int>()
    override val currentPlayerId = MutableLiveData<Int>()
    override val isConnected = MutableLiveData(true)

    override val numPlayers = 4
    override val players = MutableLiveData(Array(numPlayers){ PlayerModel() })
    override val numCardsPerDeck = 4
    var userRelativeId: Int? = null
    var hostId: Int? = null
    override val userId = 0
    var computerMoveTimer: Timer? = null
    // Variable to make sure setup is only done exactly once throughout user's entire time in the PlayScreen
    // Do not change it back to false outside of its initial value here when the view model is created
    // It would risk setup being repeated unexpectedly due to recomposition
    var isSetupDone = false

    private fun postGameDataValues(gameData: PlayGameModel, userRelativeId: Int): Boolean {
        if (gameData.currentPlayerId != null) {
            drawPile.postValue(gameData.drawPile)
            playersDeck.postValue(gameData.playersDecks.rotateFromRemote(userRelativeId))
            playersDiscardPile.postValue(gameData.playersDiscardPiles.rotateFromRemote(userRelativeId))
            playersTurnStatus.postValue(gameData.playersTurnStatuses.rotateFromRemote(userRelativeId))
            turnStatus.postValue(gameData.playersTurnStatuses[userRelativeId])
            gameStatus.postValue(gameData.gameStatus)
            playersScore.postValue(gameData.playersScores.rotateFromRemote(userRelativeId))
            currentPlayerId.postValue(gameData.currentPlayerId.rotateFromRemote(userRelativeId))
            return true
        }
        return false
    }

    fun isHost(): Boolean {
        return userId == hostId
    }

    suspend fun getGameFromLobby(id: String) {
        val game = lobbyRepository.getGameById(id)
        if (game != null) {
            startingGameData.postValue(game)
        }
    }

    suspend fun getGameFromOngoingGames(id: String) {
        val game = onlinePlayRepository.getGameById(id)
        if (game != null) {
            startingGameData.postValue(game)
        }
    }

    override fun syncGameData(gameId: String) {
        onlinePlayRepository.syncGameData(gameId) { gameData ->
            val userRelativeId = this.userRelativeId
            if (userRelativeId != null) {
                postGameDataValues(gameData, userRelativeId)
            }
        }
        onlinePlayRepository.checkConnectionStatus {
            isConnected.postValue(it)
        }
    }

    override fun setPlayers(userKey: String, gameData: LobbyGameModel) {
        val listPlayers = gameData.listPlayers
        val userRelativeId = listPlayers.map { it.id }.indexOf(userKey)
        players.postValue(generatePlayersArrayUseCase(listPlayers.map { it.name }, userRelativeId))

        this.hostId = listPlayers.lastIndex.rotateFromRemote(userRelativeId)
        this.userRelativeId = userRelativeId

        enterMultiPlayerGameUseCase(gameData.id, userKey, listPlayers.size)

        isSetupDone = true
    }

    override suspend fun startGame(gameId: String) {
        val gameData = startMultiPlayerGameUseCase(gameId, numPlayers, numCardsPerDeck)
        if (gameData != null) {
            val currentPlayerId = gameData.currentPlayerId
            val donePostGameDataValues = postGameDataValues(gameData, this.userRelativeId!!)
            if (
                donePostGameDataValues && currentPlayerId != null
                && !players.value!![currentPlayerId].isHuman && isHost()
            ) {
                if (computerMoveTimer != null) {
                    computerMoveTimer?.cancel()
                }

                computerMoveTimer = Timer()
                computerMoveTimer?.schedule(timerTask{computerMove(currentPlayerId)}, 1000)
            }
        }
        syncGameData(gameId)
    }

    // Returns the updated players' decks after a card is drawn
    override fun drawCard(
        playerDeck: PlayerDeck,
        drawnCard: CardModel
    ): Array<PlayerDeck> {
        val updatedPlayerDeck = updatePlayerDeck(playerDeck) { this.add(drawnCard) }
        val updatedPlayersDeck = playersDeck.value!!
        updatedPlayersDeck.setById(playerDeck.id, updatedPlayerDeck)
        return updatedPlayersDeck // Jangan di rotate dulu
    }

    override fun drawCard(playerDeck: PlayerDeck, drawSource: DrawSource, onFinish: () -> Unit) {
        when (drawSource) {
            DrawSource.DRAW_PILE -> drawFromDrawPile(playerDeck, drawPile.value!!.last(), onFinish)
            DrawSource.DISCARD_PILE -> drawFromDiscardPile(playerDeck, playersDiscardPile.value!!.getById(playerDeck.id).listCard.last(), onFinish)
        }
    }

    override fun drawFromDrawPile(
        playerDeck: PlayerDeck,
        drawnCard: CardModel,
        onDrawFinish: () -> Unit
    ) {
        val playerId = playerDeck.id
        val updatedPlayersDeck = drawCard(playerDeck, drawnCard)
        val updatedDrawPile = drawPile.value!!
        updatedDrawPile.remove(drawnCard)
        onlinePlayRepository.drawFromDrawPile(
            updatedPlayersDeck.rotateToRemote(userRelativeId!!),
            updatedDrawPile,
            playerId.rotateToRemote(userRelativeId!!)
        ) {
            onDrawFinish()
        }
    }

    override fun drawFromDiscardPile(
        playerDeck: PlayerDeck,
        drawnCard: CardModel,
        onDrawFinish: () -> Unit
    ) {
        val playerId = playerDeck.id
        val updatedPlayersDeck = drawCard(playerDeck, drawnCard)
        val updatedPlayersDiscardPile = updatePlayersDiscardPile(playerId) {
            this.listCard.remove(drawnCard)
        }
        onlinePlayRepository.drawFromDiscardPile(
            updatedPlayersDeck.rotateToRemote(userRelativeId!!),
            updatedPlayersDiscardPile.rotateToRemote(userRelativeId!!),
            playerId.rotateToRemote(userRelativeId!!)
        ) {
            onDrawFinish()
        }
    }

    override fun discardCard(playerDeck: PlayerDeck, discardedCard: CardModel) {
        val updatedCurrentPlayerDeck = updatePlayerDeck(playerDeck) { this.remove(discardedCard) }
        val updatedPlayersDeck = playersDeck.value!!
        updatedPlayersDeck.setById(playerDeck.id, updatedCurrentPlayerDeck)

        val nextPlayerId = (playerDeck.id + 1) % numPlayers
        val updatedPlayersDiscardPile = updatePlayersDiscardPile(nextPlayerId) {
            this.listCard.add(discardedCard)
        }

        onlinePlayRepository.discardCard(
            updatedPlayersDeck.rotateToRemote(userRelativeId!!),
            updatedPlayersDiscardPile.rotateToRemote(userRelativeId!!)
        ) {
            if (!checkShouldGameEnd(updatedCurrentPlayerDeck)) {
                onlinePlayRepository.endTurn(playerDeck.id.rotateToRemote(userRelativeId!!)) {
                    if (!players.value!![nextPlayerId].isHuman && isHost()) {
                        Timer().schedule(timerTask { computerMove(nextPlayerId) }, 1000)
                    }
                }
            } else {
                endGame()
            }
        }
    }

    override fun computerMove(id: Int) {
        if (drawPile.value.isNullOrEmpty()) {
            return
        }

        val computerMove = computerMoveUseCase(
            playersDeck.value!!.getById(id), playersDiscardPile.value!!.getById(id)
        )

        if (computerMoveTimer != null) {
            computerMoveTimer?.cancel()
        }

        computerMoveTimer = Timer()
        computerMoveTimer?.schedule(1000) {
            drawCard(playersDeck.value!!.getById(id), computerMove.first) {
                Timer().schedule(1000) {
                    discardCard(playersDeck.value!!.getById(id), computerMove.second)
                }
            }
        }
    }

    override fun endGame() {
        val userRelativeId = this.userRelativeId
        if (userRelativeId != null) {
            endOnlineGameUseCase(startingGameData.value!!.id,  playersDeck.value!!, userRelativeId)
        }
    }

    override fun restartGame() {
        onlinePlayRepository.updateGameStatus(GameStatus.STARTING)
    }

    fun leaveGame(gameId: String, userKey: String) {
        onlinePlayRepository.leaveGame(gameId, userKey)
    }
}