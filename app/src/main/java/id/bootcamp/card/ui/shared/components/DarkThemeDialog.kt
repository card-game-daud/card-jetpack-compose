package id.bootcamp.card.ui.shared.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import id.bootcamp.card.ui.theme.CardTheme
import id.bootcamp.card.ui.theme.ChosenTheme

@Composable
fun DarkThemeDialog(
    currentTheme: ChosenTheme,
    onDismissRequest: () -> Unit,
    onConfirmChange: (ChosenTheme) -> Unit,
    modifier: Modifier = Modifier
) {
    Dialog(
        onDismissRequest = onDismissRequest
    ) {
        Card(modifier = modifier) {
            var selectedTheme by rememberSaveable {
                mutableStateOf(currentTheme)
            }
            Text(
                text = "Change Theme",
                style = MaterialTheme.typography.titleLarge,
                modifier = Modifier.padding(16.dp)
            )
            ThemeRadioButton(
                selected = selectedTheme == ChosenTheme.DARK,
                onClick = { selectedTheme = ChosenTheme.DARK },
                text = "Dark",
                modifier = Modifier.padding(8.dp, 0.dp)
            )
            ThemeRadioButton(
                selected = selectedTheme == ChosenTheme.LIGHT,
                onClick = { selectedTheme = ChosenTheme.LIGHT },
                text = "Light",
                modifier = Modifier.padding(8.dp, 0.dp)
            )
            ThemeRadioButton(
                selected = selectedTheme == ChosenTheme.SYSTEM,
                onClick = { selectedTheme = ChosenTheme.SYSTEM },
                text = "System",
                modifier = Modifier.padding(8.dp, 0.dp)
            )
            Row(
                modifier = Modifier
                    .padding(16.dp)
                    .align(Alignment.CenterHorizontally),
                horizontalArrangement = Arrangement.spacedBy(8.dp)
            ) {
                Button(onClick = {
                    if (selectedTheme != currentTheme) onConfirmChange(selectedTheme)
                    onDismissRequest()
                }) {
                    Text(text = "OK")
                }
                OutlinedButton(onClick = onDismissRequest) {
                    Text(text = "Cancel")
                }
            }
        }
    }
}

@Preview
@Composable
fun DarkThemeDialogPreview() {
    CardTheme {
        DarkThemeDialog(currentTheme = ChosenTheme.SYSTEM, onDismissRequest = { }, onConfirmChange = { })
    }
}