package id.bootcamp.card.ui.viewmodel

import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.bootcamp.card.domain.model.AnonymousUserModel
import id.bootcamp.card.domain.model.LobbyGameModel
import id.bootcamp.card.domain.repository.LobbyRepository
import id.bootcamp.card.domain.usecase.EnterLobbyUseCase
import kotlinx.coroutines.launch

class LobbyViewModel(
    private val lobbyRepository: LobbyRepository,
    private val enterLobbyUseCase: EnterLobbyUseCase
): ViewModel() {
    val userData = MutableLiveData<AnonymousUserModel>()
    val lobbyGameData = MutableLiveData<SnapshotStateList<LobbyGameModel>>()
    val isConnected = MutableLiveData<Boolean>()

    fun enterLobby() = viewModelScope.launch {
        enterLobbyUseCase(
            onGetUser = { user -> userData.postValue(user); println("Update lobby user: ${userData.value?.name}") },
            onGetLobbyGames = { lobbyGamesList -> lobbyGameData.postValue(lobbyGamesList); println("Update lobby data: ${lobbyGameData.value?.size}") },
            onConnectionStatusChange = { newConnectionStatus -> isConnected.postValue(newConnectionStatus)}
        )
    }

    fun addGame(user: AnonymousUserModel) {
        lobbyRepository.addGame(user)
    }

    fun joinGame(userData: AnonymousUserModel, gameId: String) {
        lobbyRepository.joinGame(userData, gameId)
    }

    fun leaveGame(userId: String, gameId: String) {
        lobbyRepository.leaveGame(userId, gameId)
    }

    fun checkIsUserInGame(userId: String, lobby: SnapshotStateList<LobbyGameModel>): Boolean {
        for (game in lobby) {
            if (userId in game.listPlayersId) {
                return true
            }
        }
        return false
    }

    fun startGame(gameId: String) {
        lobbyRepository.startGame(gameId)
    }
}