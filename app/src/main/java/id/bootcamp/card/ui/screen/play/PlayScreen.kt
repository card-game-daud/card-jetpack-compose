package id.bootcamp.card.ui.screen.play

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.lifecycle.viewmodel.compose.viewModel
import id.bootcamp.card.domain.model.GameStatus
import id.bootcamp.card.domain.model.LobbyGameModel
import id.bootcamp.card.domain.model.TurnStatus
import id.bootcamp.card.ui.shared.components.DisconnectedDialog
import id.bootcamp.card.ui.screen.play.components.ConfirmLeaveDialog
import id.bootcamp.card.ui.screen.play.components.GameOverDialog
import id.bootcamp.card.ui.screen.play.components.PlayArea
import id.bootcamp.card.ui.screen.play.components.SaveOptionsDialog
import id.bootcamp.card.ui.theme.CardTheme
import id.bootcamp.card.ui.viewmodel.MainViewModelFactory
import id.bootcamp.card.ui.viewmodel.MultiPlayerViewModel
import id.bootcamp.card.ui.viewmodel.OfflinePlayViewModel
import id.bootcamp.card.ui.viewmodel.OnlinePlayViewModel
import id.bootcamp.card.ui.viewmodel.PlayViewModel

@Composable
fun PlayScreen(
    viewModel: PlayViewModel,
    startingGameData: LobbyGameModel? = null,
    userId: String = "",
    onLeaveGame: () -> Unit
) {
    var showSaveGameOptionsDialog by rememberSaveable {
        mutableStateOf(false)
    }
    var showLeaveConfirmationDialog by rememberSaveable {
        mutableStateOf(false)
    }
    var showGameOverDialog by rememberSaveable {
        mutableStateOf(false)
    }
    var showDisconnectedDialog by rememberSaveable {
        mutableStateOf(false)
    }
    fun hideAllDialogsFromLeaveConfirmationDialog() {
        showLeaveConfirmationDialog = false
        showGameOverDialog = false
    }
    fun hideAllDialogsFromSaveOptionsDialog() {
        showSaveGameOptionsDialog = false
        hideAllDialogsFromLeaveConfirmationDialog()
    }

    val playersLiveData by viewModel.players.observeAsState()
    val playersDeckLiveData by viewModel.playersDeck.observeAsState()
    val playersDiscardPileLiveData by viewModel.playersDiscardPile.observeAsState()
    val drawPileLiveData by viewModel.drawPile.observeAsState()
    val turnStatusLiveData by viewModel.turnStatus.observeAsState()
    val gameStatus by viewModel.gameStatus.observeAsState()
    val playersScoreLiveData by viewModel.playersScore.observeAsState()
    val currentPlayerIdLiveData by viewModel.currentPlayerId.observeAsState()
    val isConnected by viewModel.isConnected.observeAsState()

    // For smart cast purpose
    val playersData = playersLiveData
    val playersDeck = playersDeckLiveData
    val drawPile = drawPileLiveData
    val playersDiscardPile = playersDiscardPileLiveData
    val turnStatus = turnStatusLiveData
    val playersScore = playersScoreLiveData
    val currentPlayerId = currentPlayerIdLiveData

    // Should be only executed when isConnected value changes
    // Otherwise, it interferes with DisconnectedDialog's onClickBack
    LaunchedEffect(key1 = isConnected) {
        showDisconnectedDialog = (isConnected == false)
    }

    if (showDisconnectedDialog) {
        Surface(Modifier.fillMaxSize()) {
            DisconnectedDialog(
                canNavigateBack = viewModel !is MultiPlayerViewModel,
                onClickBack = {
                    showDisconnectedDialog = false
                    onLeaveGame()
                }
            )
        }
    }

    BackHandler {
        // In multiplayer, players can only leave through the game over dialog
        if (viewModel !is MultiPlayerViewModel) {
            showLeaveConfirmationDialog = true
        }
    }
    if (showLeaveConfirmationDialog) {
        ConfirmLeaveDialog(
            askToSave = viewModel is OnlinePlayViewModel,
            onConfirm = {
                if (viewModel !is OnlinePlayViewModel) {
                    hideAllDialogsFromLeaveConfirmationDialog()
                    onLeaveGame()
                } else {
                    showSaveGameOptionsDialog = true
                }
            },
            onDismiss = {
                showLeaveConfirmationDialog = false
            }
        )
    }
    if (showSaveGameOptionsDialog && viewModel is OnlinePlayViewModel) {
        SaveOptionsDialog(
            onDelete = { viewModel.deleteGame() },
            onLeave = {
                hideAllDialogsFromSaveOptionsDialog()
                onLeaveGame()
            },
            onDismiss = { showSaveGameOptionsDialog = false }
        )
    }

    if (gameStatus == GameStatus.STARTING) {
        LaunchedEffect(key1 = Unit) {
            viewModel.setupGame(startingGameData, userId)
        }
    } else if (gameStatus == GameStatus.FINISHING) {
        LaunchedEffect(key1 = Unit) {
            showGameOverDialog = true
        }
    }
    if (
        showGameOverDialog
        && playersData != null && playersData.size == viewModel.numPlayers
        && playersScore != null && playersScore.size == viewModel.numPlayers
        && playersDeck != null && playersDeck.size == viewModel.numPlayers
    ) {
        GameOverDialog(
            playerNames = playersData.map { it.name }.toTypedArray(),
            playersDeck = playersDeck,
            playersScore = playersScore,
            isRestartable = viewModel !is MultiPlayerViewModel,
            onClickRestart = {
                viewModel.restartGame()
            },
            onClickExit = {
                if (viewModel !is MultiPlayerViewModel) {
                    showLeaveConfirmationDialog = true
                } else {
                    showGameOverDialog = false
                    viewModel.leaveGame(startingGameData?.id ?: "", userId)
                    onLeaveGame()
                }
            }
        )
    }

    if (
        playersData != null && playersDeck != null && playersDiscardPile != null
        && drawPile != null && currentPlayerId != null
    ) {
        PlayArea(
            playerNames = playersData.map { it.name }.toTypedArray(),
            playersDeck = playersDeck,
            playersDiscardPile = playersDiscardPile,
            drawPile = drawPile,
            currentPlayerId = currentPlayerId,
            onDrawFromDiscardPile = { card ->
                if (turnStatus == TurnStatus.DRAWING && playersDeck[0].listCard.size == 4)
                    viewModel.drawFromDiscardPile(playersDeck[0], card)
            },
            onDrawFromDrawPile = { card ->
                if (turnStatus == TurnStatus.DRAWING && playersDeck[0].listCard.size == 4)
                    viewModel.drawFromDrawPile(playersDeck[0], card)
            },
            onDiscardCard = { card ->
                if (turnStatus == TurnStatus.DISCARDING && playersDeck[0].listCard.size == 5)
                    viewModel.discardCard(playersDeck[0], card)
            }
        )
    }
}

@Preview
@Composable
fun PlayScreenPreview() {
    CardTheme {
        val context = LocalContext.current
        val viewModel = viewModel(
            modelClass = OfflinePlayViewModel::class.java,
            factory = MainViewModelFactory.getInstance(context)
        )
        PlayScreen(viewModel = viewModel, onLeaveGame = {})
    }
}