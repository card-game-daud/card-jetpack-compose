package id.bootcamp.card.ui.screen.lobby.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Person
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedButton
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import id.bootcamp.card.ui.theme.CardTheme

@Composable
fun LobbyTopBar(
    currentUserName: String
) {
    Surface(
        color = MaterialTheme.colorScheme.surfaceVariant,
        modifier = Modifier.fillMaxWidth()
    ) {
        Row(
            horizontalArrangement = Arrangement.SpaceBetween,
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                text = "Lobby",
                style = MaterialTheme.typography.titleLarge,
                modifier = Modifier.padding(16.dp)
            )
            LobbyTopBarNameButton(
                currentUserName = currentUserName,
                modifier = Modifier.padding(8.dp)
            )
        }
    }
}

@Composable
fun LobbyTopBarNameButton(
    currentUserName: String,
    modifier: Modifier = Modifier
) {
    OutlinedButton(
        onClick = {  },
        modifier = modifier,
        colors = ButtonDefaults.outlinedButtonColors(contentColor = MaterialTheme.colorScheme.onSurfaceVariant)
    ) {
        Icon(imageVector = Icons.Default.Person, contentDescription = currentUserName)
        Spacer(modifier = Modifier.width(8.dp))
        Text(
            text = currentUserName
        )
    }
}

@Preview
@Composable
fun LobbyTopBarPreview() {
    CardTheme {
        LobbyTopBar(currentUserName = "tes")
    }
}