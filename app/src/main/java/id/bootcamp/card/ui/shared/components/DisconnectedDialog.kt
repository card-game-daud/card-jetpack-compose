package id.bootcamp.card.ui.shared.components

import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import id.bootcamp.card.ui.theme.CardTheme

@Composable
fun DisconnectedDialog(
    canNavigateBack: Boolean,
    onClickBack: () -> Unit,
    modifier: Modifier = Modifier
) {
    Dialog(
        onDismissRequest = { /* Undismissable by normal means (see properties) */ },
        properties = DialogProperties(dismissOnBackPress = false, dismissOnClickOutside = false)
    ) {
        Card(modifier = modifier) {
            Text(
                text = "Lost Connection",
                style = MaterialTheme.typography.titleLarge,
                modifier = Modifier.padding(16.dp)
            )
            Text(
                text = "You have lost connection to the database. Make sure you are connected to the Internet"
                        + if (canNavigateBack) "or click below to return to the previous screen" else ""
                        + ".",
                textAlign = TextAlign.Justify,
                modifier = Modifier.padding(16.dp, 8.dp)
            )
            if (canNavigateBack) {
                Button(
                    onClick = onClickBack,
                    modifier = Modifier
                        .padding(16.dp)
                        .align(Alignment.CenterHorizontally)
                ) {
                    Text(text = "Go Back")
                }
            } else {
                // Replacement for button's margin
                Spacer(modifier = Modifier.height(8.dp))
            }
        }
    }
}

@Preview
@Composable
fun DisconnectedDialogPreview() {
    CardTheme {
        DisconnectedDialog(false, onClickBack = {})
    }
}