package id.bootcamp.card.ui.viewmodel

import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import id.bootcamp.card.domain.model.CardModel
import id.bootcamp.card.domain.model.DrawSource
import id.bootcamp.card.domain.model.GameStatus
import id.bootcamp.card.domain.model.LobbyGameModel
import id.bootcamp.card.domain.model.PlayGameModel
import id.bootcamp.card.domain.model.PlayerDeck
import id.bootcamp.card.domain.model.PlayerDiscardPile
import id.bootcamp.card.domain.model.PlayerModel
import id.bootcamp.card.domain.model.TurnStatus
import id.bootcamp.card.domain.repository.OnlinePlayRepository
import id.bootcamp.card.domain.usecase.ComputerMoveUseCase
import id.bootcamp.card.domain.usecase.EndOnlineGameUseCase
import id.bootcamp.card.domain.usecase.GeneratePlayersArrayUseCase
import id.bootcamp.card.domain.usecase.GetAnonymousUserUseCase
import id.bootcamp.card.domain.usecase.StartOnlineGameUseCase
import id.bootcamp.card.utils.GameDefaults
import java.util.Timer
import kotlin.concurrent.schedule
import kotlin.concurrent.timerTask

class OnlinePlayViewModel(
    private val onlinePlayRepository: OnlinePlayRepository,
    private val getAnonymousUserUseCase: GetAnonymousUserUseCase,
    private val generatePlayersArrayUseCase: GeneratePlayersArrayUseCase,
    private val startOnlineGameUseCase: StartOnlineGameUseCase,
    private val endOnlineGameUseCase: EndOnlineGameUseCase,
    private val computerMoveUseCase: ComputerMoveUseCase
): ViewModel(), PlayViewModel {
    override val drawPile = MutableLiveData<SnapshotStateList<CardModel>>()
    override val playersDeck = MutableLiveData<Array<PlayerDeck>>()
    override val playersDiscardPile = MutableLiveData<Array<PlayerDiscardPile>>()
    val playersTurnStatus = MutableLiveData<Array<TurnStatus>>()
    override val turnStatus = MutableLiveData<TurnStatus>()
    override val gameStatus = MutableLiveData<GameStatus>(GameStatus.STARTING)
    override val playersScore = MutableLiveData<Array<Int>>()
    override val startingPlayer = MutableLiveData<Int>()
    override val currentPlayerId = MutableLiveData<Int>()
    override val players = MutableLiveData<Array<PlayerModel>>()
    override val isConnected = MutableLiveData(true)
    val gameId = MutableLiveData<String>()

    override val numPlayers = 4
    override val numCardsPerDeck = 4
    override var userId = 0
    var computerMoveTimer: Timer? = null

    private fun postGameDataValues(gameData: PlayGameModel): Boolean {
        drawPile.postValue(gameData.drawPile)
        playersDeck.postValue(gameData.playersDecks)
        playersDiscardPile.postValue(gameData.playersDiscardPiles)
        playersTurnStatus.postValue(gameData.playersTurnStatuses)
        turnStatus.postValue(gameData.playersTurnStatuses[0])
        gameStatus.postValue(gameData.gameStatus)
        playersScore.postValue(gameData.playersScores)
        currentPlayerId.postValue(gameData.currentPlayerId)
        return true
    }

    override fun syncGameData(gameId: String) { // With gameId being the same as userKey
        onlinePlayRepository.syncGameData(gameId) {
            postGameDataValues(it)
        }
        onlinePlayRepository.checkConnectionStatus {
            isConnected.postValue(it)
        }
    }

    // In single-player online game, gameId is the same as userKey
    override suspend fun startGame(gameId: String) {
        val savedGame = onlinePlayRepository.getGameById(
            getAnonymousUserUseCase()?.id ?: GameDefaults.ID
        )
        if (savedGame?.id == LobbyGameModel.NONE_ID) {
            val gameData = startOnlineGameUseCase(null, numPlayers, numCardsPerDeck)
            val currentPlayerId = gameData.currentPlayerId
            val donePostGameDataValues = postGameDataValues(gameData)
            if (donePostGameDataValues && currentPlayerId != 0 && currentPlayerId != null) {
                if (computerMoveTimer != null) {
                    computerMoveTimer?.cancel()
                }

                computerMoveTimer = Timer()
                computerMoveTimer?.schedule(timerTask{computerMove(currentPlayerId)}, 1000)
            }
        }
        val currentUser = getAnonymousUserUseCase()
        this.gameId.postValue(currentUser?.id)
        players.postValue(
            generatePlayersArrayUseCase(listOf(currentUser?.name ?: "HUMAN"), 0)
        )
        syncGameData(currentUser?.id ?: GameDefaults.ID)
    }

    // Returns the updated players' decks after a card is drawn
    override fun drawCard(playerDeck: PlayerDeck, drawnCard: CardModel): Array<PlayerDeck> {
        val updatedPlayerDeck = updatePlayerDeck(playerDeck) { this.add(drawnCard) }
        val updatedPlayersDeck = playersDeck.value!!
        updatedPlayersDeck[playerDeck.id] = updatedPlayerDeck
        return updatedPlayersDeck
    }

    override fun drawCard(playerDeck: PlayerDeck, drawSource: DrawSource, onFinish: () -> Unit) {
        when (drawSource) {
            DrawSource.DRAW_PILE -> drawFromDrawPile(playerDeck, drawPile.value!!.last(), onFinish)
            DrawSource.DISCARD_PILE -> drawFromDiscardPile(playerDeck, playersDiscardPile.value!![playerDeck.id].listCard.last(), onFinish)
        }
    }

    override fun drawFromDrawPile(playerDeck: PlayerDeck, drawnCard: CardModel, onDrawFinish: () -> Unit) {
        val updatedPlayersDeck = drawCard(playerDeck, drawnCard)
        val updatedDrawPile = drawPile.value!!
        updatedDrawPile.remove(drawnCard)
        onlinePlayRepository.drawFromDrawPile(updatedPlayersDeck, updatedDrawPile, playerDeck.id) {
            onDrawFinish()
        }
    }

    override fun drawFromDiscardPile(playerDeck: PlayerDeck, drawnCard: CardModel, onDrawFinish: () -> Unit) {
        val updatedPlayersDeck = drawCard(playerDeck, drawnCard)
        val updatedPlayersDiscardPile = updatePlayersDiscardPile(playerDeck.id) {
            this.listCard.remove(drawnCard)
        }
        onlinePlayRepository.drawFromDiscardPile(updatedPlayersDeck, updatedPlayersDiscardPile, playerDeck.id) {
            onDrawFinish()
        }
    }

    override fun discardCard(playerDeck: PlayerDeck, discardedCard: CardModel) {
        val updatedCurrentPlayerDeck = updatePlayerDeck(playerDeck) { this.remove(discardedCard) }
        val updatedPlayersDeck = playersDeck.value!!
        updatedPlayersDeck[playerDeck.id] = updatedCurrentPlayerDeck

        val nextPlayerId = (playerDeck.id + 1) % numPlayers
        val updatedPlayersDiscardPile = updatePlayersDiscardPile(nextPlayerId) {
            this.listCard.add(discardedCard)
        }

        onlinePlayRepository.discardCard(
            updatedPlayersDeck,
            updatedPlayersDiscardPile
        ) {
            if (!checkShouldGameEnd(updatedCurrentPlayerDeck)) {
                onlinePlayRepository.endTurn(playerDeck.id) {
                    if (nextPlayerId != userId) {
                        Timer().schedule(timerTask { computerMove(nextPlayerId) }, 1000)
                    }
                }
            } else {
                endGame()
            }
        }
    }

    override fun computerMove(id: Int) {
        if (drawPile.value.isNullOrEmpty()) {
            return
        }

        val computerMove = computerMoveUseCase(
            playersDeck.value!![id], playersDiscardPile.value!![id]
        )

        if (computerMoveTimer != null) {
            computerMoveTimer?.cancel()
        }

        computerMoveTimer = Timer()
        computerMoveTimer?.schedule(1000) {
            drawCard(playersDeck.value!![id], computerMove.first) {
                Timer().schedule(1000) {
                    discardCard(playersDeck.value!![id], computerMove.second)
                }
            }
        }
    }

    override fun endGame() {
        endOnlineGameUseCase(gameId.value ?: GameDefaults.ID, playersDeck.value!!)
    }

    override fun restartGame() {
        onlinePlayRepository.updateGameStatus(GameStatus.STARTING)
    }

    fun deleteGame() {
        // The gameId and userId of an online single-player game are the same
        onlinePlayRepository.leaveGame(gameId.value!!, gameId.value!!)
    }
}