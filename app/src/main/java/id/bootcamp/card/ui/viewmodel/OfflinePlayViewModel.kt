package id.bootcamp.card.ui.viewmodel

import androidx.compose.runtime.snapshots.SnapshotStateList
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import id.bootcamp.card.domain.model.CardModel
import id.bootcamp.card.domain.model.DrawSource
import id.bootcamp.card.domain.model.GameStatus
import id.bootcamp.card.domain.model.PlayerDeck
import id.bootcamp.card.domain.model.PlayerDiscardPile
import id.bootcamp.card.domain.model.PlayerModel
import id.bootcamp.card.domain.model.TurnStatus
import id.bootcamp.card.domain.usecase.ComputerMoveUseCase
import id.bootcamp.card.domain.usecase.GenerateDeckUseCase
import id.bootcamp.card.domain.usecase.GeneratePlayersArrayUseCase
import id.bootcamp.card.domain.usecase.GetAnonymousUserUseCase
import java.util.Timer
import kotlin.concurrent.timerTask

class OfflinePlayViewModel(
    private val getAnonymousUserUseCase: GetAnonymousUserUseCase,
    private val generatePlayersArrayUseCase: GeneratePlayersArrayUseCase,
    private val generateDeckUseCase: GenerateDeckUseCase,
    private val computerMoveUseCase: ComputerMoveUseCase
): ViewModel(), PlayViewModel {
    override val drawPile = MutableLiveData<SnapshotStateList<CardModel>>()
    override val playersDeck = MutableLiveData<Array<PlayerDeck>>()
    override val playersDiscardPile = MutableLiveData<Array<PlayerDiscardPile>>()
    override val turnStatus = MutableLiveData<TurnStatus>()
    override val gameStatus = MutableLiveData<GameStatus>(GameStatus.STARTING)
    override val playersScore = MutableLiveData<Array<Int>>()
    override val startingPlayer = MutableLiveData<Int>()
    override val currentPlayerId = MutableLiveData<Int>()
    override val isConnected = MutableLiveData(true)

    override val numPlayers = 4
    override val players = MutableLiveData(Array(numPlayers) {
        if (it == userId) PlayerModel(name = "HUMAN")
        else PlayerModel(name = "COMP $it")
    })
    override val numCardsPerDeck = 4
    override val userId = 0

    // gameId is for override purpose only
    override suspend fun startGame(gameId: String) {
        val (newPlayersDeck, newDrawPile) =
            generateDeckUseCase(numPlayers, numCardsPerDeck)
        val newStartingPlayer = (0..3).random()
        val userTurnStatus = if (newStartingPlayer == userId)
            TurnStatus.DRAWING
        else TurnStatus.WAITING

        players.postValue(
            generatePlayersArrayUseCase(listOf(getAnonymousUserUseCase()?.name ?: "HUMAN"), userId)
        )
        playersDeck.postValue(newPlayersDeck)
        drawPile.postValue(newDrawPile)
        playersDiscardPile.postValue(Array(numPlayers){ PlayerDiscardPile(it, SnapshotStateList()) })
        turnStatus.postValue(userTurnStatus)
        gameStatus.postValue(GameStatus.ONGOING)
        playersScore.postValue(null)
        startingPlayer.postValue(newStartingPlayer)
        currentPlayerId.postValue(newStartingPlayer)

        if (newStartingPlayer != userId) {
            Timer().schedule(timerTask { computerMove(newStartingPlayer) }, 1000)
        }
    }

    // Returns the updated players' decks after a card is drawn
    //  The return value is not necessary for this case as postValue can be done immediately here
    override fun drawCard(playerDeck: PlayerDeck, drawnCard: CardModel): Array<PlayerDeck> {
        val updatedPlayerDeck = updatePlayerDeck(playerDeck) { this.add(drawnCard) }
        val updatedPlayersDeck = playersDeck.value!!
        updatedPlayersDeck[playerDeck.id] = updatedPlayerDeck
        playersDeck.postValue(updatedPlayersDeck)
        turnStatus.postValue(TurnStatus.DISCARDING)
        return updatedPlayersDeck
    }

    override fun drawCard(playerDeck: PlayerDeck, drawSource: DrawSource, onFinish: () -> Unit) {
        when (drawSource) {
            DrawSource.DRAW_PILE -> drawFromDrawPile(playerDeck, drawPile.value!!.last())
            DrawSource.DISCARD_PILE -> drawFromDiscardPile(playerDeck, playersDiscardPile.value!![playerDeck.id].listCard.last())
        }
    }

    override fun drawFromDrawPile(playerDeck: PlayerDeck, drawnCard: CardModel, onDrawFinish: () -> Unit) {
        drawCard(playerDeck, drawnCard)
        val updatedDrawPile = drawPile.value!!
        updatedDrawPile.remove(drawnCard)
        drawPile.postValue(updatedDrawPile)
    }

    override fun drawFromDiscardPile(playerDeck: PlayerDeck, drawnCard: CardModel, onDrawFinish: () -> Unit) {
        drawCard(playerDeck, drawnCard)
        val updatedPlayersDiscardPile = updatePlayersDiscardPile(playerDeck.id) {
            this.listCard.remove(drawnCard)
        }
        playersDiscardPile.postValue(updatedPlayersDiscardPile)
    }

    override fun discardCard(playerDeck: PlayerDeck, discardedCard: CardModel) {
        val updatedCurrentPlayerDeck = updatePlayerDeck(playerDeck) { this.remove(discardedCard) }
        val updatedPlayersDeck = playersDeck.value!!
        updatedPlayersDeck[playerDeck.id] = updatedCurrentPlayerDeck
        playersDeck.postValue(updatedPlayersDeck)

        val nextPlayerId = (playerDeck.id + 1) % numPlayers
        val updatedPlayersDiscardPile = updatePlayersDiscardPile(nextPlayerId) {
            this.listCard.add(discardedCard)
        }
        playersDiscardPile.postValue(updatedPlayersDiscardPile)

        if (!checkShouldGameEnd(updatedCurrentPlayerDeck)) {
            turnStatus.postValue(TurnStatus.WAITING)
            currentPlayerId.postValue(nextPlayerId)
            if (playerDeck.id == userId) {
                Timer().schedule(timerTask { computerMove(nextPlayerId) }, 1000)
            }
        } else {
            endGame()
        }
    }

    override fun computerMove(id: Int) {
        if (drawPile.value!!.isEmpty()) {
            return
        }

        val computerMove = computerMoveUseCase(
            playersDeck.value!![id], playersDiscardPile.value!![id]
        )
        drawCard(playersDeck.value!![id], computerMove.first)
        discardCard(playersDeck.value!![id], computerMove.second)

        val nextPlayerId = (id + 1) % numPlayers
        if (nextPlayerId == userId) {
            turnStatus.postValue(TurnStatus.DRAWING)
        } else {
            Timer().schedule(timerTask { computerMove(nextPlayerId) }, 1000)
        }
    }

    override fun endGame() {
        gameStatus.postValue(GameStatus.FINISHING)
        calculateAllPlayersScore()
    }

    override fun restartGame() {
        gameStatus.postValue(GameStatus.STARTING)
    }
}