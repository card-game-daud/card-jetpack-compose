package id.bootcamp.card.ui.screen.home.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Button
import androidx.compose.material3.Card
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog

@Composable
fun SinglePlayerChoicesDialog(
    onDismissRequest: () -> Unit,
    onChooseOffline: () -> Unit,
    onChooseOnline: () -> Unit
) {
    Dialog(onDismissRequest = onDismissRequest) {
        Card {
            Text(
                text = "Single Player",
                style = MaterialTheme.typography.titleLarge,
                modifier = Modifier
                    .padding(16.dp, 16.dp, 16.dp, 8.dp)
                    .align(Alignment.CenterHorizontally)
            )
            Row(
                horizontalArrangement = Arrangement.spacedBy(16.dp, Alignment.CenterHorizontally),
                modifier = Modifier.padding(16.dp, 8.dp, 16.dp, 16.dp)
            ) {
                OfflineSinglePlayerButton(onClick = {
                    onDismissRequest()
                    onChooseOffline()
                })
                OnlineSinglePlayerButton(onClick = {
                    onDismissRequest()
                    onChooseOnline()
                })
            }
        }
    }
}

@Composable
fun OfflineSinglePlayerButton(onClick: () -> Unit) {
    Button(onClick = onClick) {
        Text("Offline")
    }
}

@Composable
fun OnlineSinglePlayerButton(onClick: () -> Unit) {
    Button(onClick = onClick) {
        Text("Online")
    }
}