package id.bootcamp.card.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import id.bootcamp.card.data.datastore.MainDataStore
import id.bootcamp.card.ui.theme.ChosenTheme
import kotlinx.coroutines.launch

class ThemeViewModel(private val dataStore: MainDataStore): ViewModel() {
    val useDarkTheme = MutableLiveData<ChosenTheme>()

    init {
        viewModelScope.launch {
            dataStore.loadTheme().collect {
                useDarkTheme.postValue(it)
            }
        }
    }

    fun changeTheme(newTheme: ChosenTheme) = viewModelScope.launch {
        dataStore.changeTheme(newTheme)
        dataStore.loadTheme().collect {
            if (it != null) useDarkTheme.postValue(it)
        }
    }
}