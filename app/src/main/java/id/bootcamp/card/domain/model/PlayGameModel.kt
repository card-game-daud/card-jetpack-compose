package id.bootcamp.card.domain.model

import androidx.compose.runtime.snapshots.SnapshotStateList

class PlayGameModel(
    val drawPile: SnapshotStateList<CardModel>,
    val playersDecks: Array<PlayerDeck>,
    val playersDiscardPiles: Array<PlayerDiscardPile>,
    val playersTurnStatuses: Array<TurnStatus>,
    val gameStatus: GameStatus?,
    val playersScores: Array<Int>,
    val currentPlayerId: Int?
)
