package id.bootcamp.card.domain.usecase

import id.bootcamp.card.domain.mapper.rotateFromRemote
import id.bootcamp.card.domain.mapper.rotateToRemote
import id.bootcamp.card.domain.model.PlayerModel

class GeneratePlayersArrayUseCase {
    operator fun invoke(humanList: List<String>, userRelativeId: Int): Array<PlayerModel> {
        val playersArray = Array(4) { PlayerModel() }
        humanList.forEachIndexed { index, key ->
            playersArray[index.rotateFromRemote(userRelativeId)].name = key
            playersArray[index.rotateFromRemote(userRelativeId)].isHuman = true
        }
        playersArray.forEachIndexed { index, player ->
            if (!player.isHuman) {
                player.name = "COMP ${index.rotateToRemote(userRelativeId)}"
            }
        }
        return playersArray
    }
}