package id.bootcamp.card.domain.usecase

import id.bootcamp.card.data.repository.OnlinePlayRepositoryImpl
import id.bootcamp.card.domain.model.LobbyGameModel

class GetCurrentGameUseCase(private val onlinePlayRepository: OnlinePlayRepositoryImpl) {
    suspend operator fun invoke(userId: String): LobbyGameModel? {
        return onlinePlayRepository.getCurrentGameOfUser(userId)
    }
}