package id.bootcamp.card.domain.model

enum class GameStatus {
    STARTING, ONGOING, FINISHING
}