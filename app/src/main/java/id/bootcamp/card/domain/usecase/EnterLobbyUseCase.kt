package id.bootcamp.card.domain.usecase

import androidx.compose.runtime.snapshots.SnapshotStateList
import id.bootcamp.card.domain.model.AnonymousUserModel
import id.bootcamp.card.domain.model.LobbyGameModel
import id.bootcamp.card.domain.repository.LobbyRepository

class EnterLobbyUseCase(
    private val getAnonymousUserUseCase: GetAnonymousUserUseCase,
    private val lobbyRepository: LobbyRepository
) {
    suspend operator fun invoke(
        onGetUser: (AnonymousUserModel) -> Unit,
        onGetLobbyGames: (SnapshotStateList<LobbyGameModel>) -> Unit,
        onConnectionStatusChange: (Boolean) -> Unit
    ) {
        val user = getAnonymousUserUseCase()
        print("sampai enterLobby: $user")
        if (user != null){
            onGetUser(user)
            lobbyRepository.getLobbyData {
                onGetLobbyGames(it)
            }
            lobbyRepository.checkConnectionStatus {
                onConnectionStatusChange(it)
            }
        }
    }
}