package id.bootcamp.card.domain.model

enum class TurnStatus {
    DRAWING, DISCARDING, WAITING
}