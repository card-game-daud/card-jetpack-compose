package id.bootcamp.card.domain.repository

import id.bootcamp.card.domain.model.AnonymousUserModel

interface AuthRepository {
    fun getCurrentUser(): AnonymousUserModel?

    suspend fun signInAnonymously(): AnonymousUserModel?

    suspend fun updateUserName(newName: String): AnonymousUserModel?
}