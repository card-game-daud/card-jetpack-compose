package id.bootcamp.card.domain.model

import androidx.compose.runtime.snapshots.SnapshotStateList

class PlayerDiscardPile constructor(val id: Int, val listCard: SnapshotStateList<CardModel>)