package id.bootcamp.card.domain.repository

import androidx.compose.runtime.snapshots.SnapshotStateList
import id.bootcamp.card.domain.model.AnonymousUserModel
import id.bootcamp.card.domain.model.LobbyGameModel

interface LobbyRepository {
    fun getLobbyData(callback: (SnapshotStateList<LobbyGameModel>) -> Unit)
    fun checkConnectionStatus(callback: (Boolean) -> Unit)
    fun addGame(user: AnonymousUserModel)
    fun joinGame(user: AnonymousUserModel, gameId: String)
    fun leaveGame(userId: String, gameId: String)
    fun startGame(gameId: String)

    suspend fun getGameById(id: String): LobbyGameModel?
    fun removeGame(gameId: String)
}