package id.bootcamp.card.domain.usecase

import id.bootcamp.card.domain.repository.OnlinePlayRepository
import id.bootcamp.card.domain.repository.LobbyRepository

class EnterMultiPlayerGameUseCase(
    private val onlinePlayRepository: OnlinePlayRepository,
    private val lobbyRepository: LobbyRepository
) {
    operator fun invoke(gameId: String, userId: String, supposedHumanCount: Int) {
        onlinePlayRepository.enterGame(gameId, userId) { humanCount ->
            if (humanCount == supposedHumanCount) {
                lobbyRepository.removeGame(gameId)
            }
        }
    }
}