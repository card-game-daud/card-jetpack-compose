package id.bootcamp.card.domain.usecase

import id.bootcamp.card.domain.model.LobbyGameModel
import id.bootcamp.card.domain.model.PlayGameModel
import id.bootcamp.card.domain.repository.OnlinePlayRepository

class StartOnlineGameUseCase(
    private val onlinePlayRepository: OnlinePlayRepository,
    private val generateDeckUseCase: GenerateDeckUseCase,
    private val getAnonymousUserUseCase: GetAnonymousUserUseCase
) {
    @OptIn(ExperimentalStdlibApi::class)
    suspend operator fun invoke(
        gameData: LobbyGameModel?,
        numPlayers: Int,
        numCardsPerDeck: Int
    ): PlayGameModel {
        val (startingPlayersDeck, startingDrawPile) = generateDeckUseCase(numPlayers, numCardsPerDeck)
        val playerIdRange = (0..<numPlayers)

        val startGameData = if (gameData == null) {
            val currentUser = getAnonymousUserUseCase()
            if (currentUser == null) null
            else {
                LobbyGameModel(
                    id = currentUser.id,
                    isWaiting = false,
                    listPlayers = listOf(currentUser),
                    rejoin = false
                )
            }
        } else gameData

        return onlinePlayRepository.startGame(startGameData, startingDrawPile, startingPlayersDeck, playerIdRange)
    }
}