package id.bootcamp.card.domain.model

import androidx.compose.runtime.snapshots.SnapshotStateList

data class LobbyGameModel(
    val id: String,
    var isWaiting: Boolean,
    val listPlayers: SnapshotStateList<AnonymousUserModel>,
    val rejoin: Boolean
) {
    val listPlayersId = listPlayers.map { it.id }

    constructor(
        id: String,
        isWaiting: Boolean,
        listPlayers: List<AnonymousUserModel>,
        rejoin: Boolean
    ): this(id, isWaiting, listPlayers.let {
        val snapshotStateList = SnapshotStateList<AnonymousUserModel>()
        snapshotStateList.addAll(listPlayers)
        snapshotStateList
    }, rejoin)

    companion object {
        const val NONE_ID = "none"
    }
}