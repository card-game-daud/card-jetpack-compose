package id.bootcamp.card.domain.model

import androidx.compose.runtime.snapshots.SnapshotStateList

class PlayerDeck constructor(val id: Int, val listCard: SnapshotStateList<CardModel>) {
    constructor(id: Int, listCard: List<CardModel>) : this(id, listCard.let {
        val snapshotStateList = SnapshotStateList<CardModel>()
        snapshotStateList.addAll(listCard)
        snapshotStateList }
    )
}