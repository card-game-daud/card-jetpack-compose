package id.bootcamp.card.domain.mapper

import androidx.compose.runtime.snapshots.SnapshotStateList
import com.google.firebase.database.DataSnapshot
import id.bootcamp.card.domain.model.LobbyGameModel

fun DataSnapshot.fromLobbySnapshotToGameModel(): LobbyGameModel {
    return LobbyGameModel(
        if (this.exists()) this.key ?: LobbyGameModel.NONE_ID else LobbyGameModel.NONE_ID,
        this.child("is_waiting").getValue(Boolean::class.java) ?: true,
        this.children
            .filterNot { it.key == "is_waiting" }
            .mapNotNull { it.fromLobbyToUserModel() },
        false
    )
}

fun DataSnapshot.fromLobbySnapshotToGameModelList(): SnapshotStateList<LobbyGameModel> {
    val gameStateList = SnapshotStateList<LobbyGameModel>()
    this.children.forEach {
        gameStateList.add(it.fromLobbySnapshotToGameModel())
    }
    return gameStateList
}

fun DataSnapshot.fromOngoingSnapshotToGameModel(): LobbyGameModel {
    return LobbyGameModel(
        if (this.exists()) this.key ?: LobbyGameModel.NONE_ID else LobbyGameModel.NONE_ID,
        false,
        this.child("players").children
            .filterNot { it.key?.startsWith("COMP ") ?: false }
            .mapNotNull { game -> game.fromOngoingToUserModel() },
        true
    )
}

fun DataSnapshot.fromOngoingSnapshotToGameModelList(userId: String): SnapshotStateList<LobbyGameModel> {
    val gameStateList = SnapshotStateList<LobbyGameModel>()
    this.children.filter { it.child("players").hasChild(userId) }.forEach {
        gameStateList.add(it.fromOngoingSnapshotToGameModel())
    }
    return gameStateList
}