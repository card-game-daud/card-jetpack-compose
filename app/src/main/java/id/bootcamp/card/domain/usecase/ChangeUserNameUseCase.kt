package id.bootcamp.card.domain.usecase

import id.bootcamp.card.data.repository.AuthRepositoryImpl
import id.bootcamp.card.domain.model.AnonymousUserModel

class ChangeUserNameUseCase(private val authRepository: AuthRepositoryImpl) {
    suspend operator fun invoke(newName: String): AnonymousUserModel? {
        return authRepository.updateUserName(newName)
    }
}