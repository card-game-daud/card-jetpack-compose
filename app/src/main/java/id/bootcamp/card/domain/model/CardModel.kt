package id.bootcamp.card.domain.model

import id.bootcamp.card.R

enum class CardModel(val rank: Rank, val suit: Suit, val image: Int) {
    TWO_OF_DIAMONDS(Rank.TWO, Suit.DIAMOND, R.drawable._2_of_diamonds),
    TWO_OF_CLUBS(Rank.TWO, Suit.CLUB, R.drawable._2_of_clubs),
    TWO_OF_HEARTS(Rank.TWO, Suit.HEART, R.drawable._2_of_hearts),
    TWO_OF_SPADES(Rank.TWO, Suit.SPADE, R.drawable._2_of_spades),
    THREE_OF_DIAMONDS(Rank.THREE, Suit.DIAMOND, R.drawable._3_of_diamonds),
    THREE_OF_CLUBS(Rank.THREE, Suit.CLUB, R.drawable._3_of_clubs),
    THREE_OF_HEARTS(Rank.THREE, Suit.HEART, R.drawable._3_of_hearts),
    THREE_OF_SPADES(Rank.THREE, Suit.SPADE, R.drawable._3_of_spades),
    FOUR_OF_DIAMONDS(Rank.FOUR, Suit.DIAMOND, R.drawable._4_of_diamonds),
    FOUR_OF_CLUBS(Rank.FOUR, Suit.CLUB, R.drawable._4_of_clubs),
    FOUR_OF_HEARTS(Rank.FOUR, Suit.HEART, R.drawable._4_of_hearts),
    FOUR_OF_SPADES(Rank.FOUR, Suit.SPADE, R.drawable._4_of_spades),
    FIVE_OF_DIAMONDS(Rank.FIVE, Suit.DIAMOND, R.drawable._5_of_diamonds),
    FIVE_OF_CLUBS(Rank.FIVE, Suit.CLUB, R.drawable._5_of_clubs),
    FIVE_OF_HEARTS(Rank.FIVE, Suit.HEART, R.drawable._5_of_hearts),
    FIVE_OF_SPADES(Rank.FIVE, Suit.SPADE, R.drawable._5_of_spades),
    SIX_OF_DIAMONDS(Rank.SIX, Suit.DIAMOND, R.drawable._6_of_diamonds),
    SIX_OF_CLUBS(Rank.SIX, Suit.CLUB, R.drawable._6_of_clubs),
    SIX_OF_HEARTS(Rank.SIX, Suit.HEART, R.drawable._6_of_hearts),
    SIX_OF_SPADES(Rank.SIX, Suit.SPADE, R.drawable._6_of_spades),
    SEVEN_OF_DIAMONDS(Rank.SEVEN, Suit.DIAMOND, R.drawable._7_of_diamonds),
    SEVEN_OF_CLUBS(Rank.SEVEN, Suit.CLUB, R.drawable._7_of_clubs),
    SEVEN_OF_HEARTS(Rank.SEVEN, Suit.HEART, R.drawable._7_of_hearts),
    SEVEN_OF_SPADES(Rank.SEVEN, Suit.SPADE, R.drawable._7_of_spades),
    EIGHT_OF_DIAMONDS(Rank.EIGHT, Suit.DIAMOND, R.drawable._8_of_diamonds),
    EIGHT_OF_CLUBS(Rank.EIGHT, Suit.CLUB, R.drawable._8_of_clubs),
    EIGHT_OF_HEARTS(Rank.EIGHT, Suit.HEART, R.drawable._8_of_hearts),
    EIGHT_OF_SPADES(Rank.EIGHT, Suit.SPADE, R.drawable._8_of_spades),
    NINE_OF_DIAMONDS(Rank.NINE, Suit.DIAMOND, R.drawable._9_of_diamonds),
    NINE_OF_CLUBS(Rank.NINE, Suit.CLUB, R.drawable._9_of_clubs),
    NINE_OF_HEARTS(Rank.NINE, Suit.HEART, R.drawable._9_of_hearts),
    NINE_OF_SPADES(Rank.NINE, Suit.SPADE, R.drawable._9_of_spades),
    TEN_OF_DIAMONDS(Rank.TEN, Suit.DIAMOND, R.drawable._10_of_diamonds),
    TEN_OF_CLUBS(Rank.TEN, Suit.CLUB, R.drawable._10_of_clubs),
    TEN_OF_HEARTS(Rank.TEN, Suit.HEART, R.drawable._10_of_hearts),
    TEN_OF_SPADES(Rank.TEN, Suit.SPADE, R.drawable._10_of_spades),
    JACK_OF_DIAMONDS(Rank.JACK, Suit.DIAMOND, R.drawable.jack_of_diamonds),
    JACK_OF_CLUBS(Rank.JACK, Suit.CLUB, R.drawable.jack_of_clubs),
    JACK_OF_HEARTS(Rank.JACK, Suit.HEART, R.drawable.jack_of_hearts),
    JACK_OF_SPADES(Rank.JACK, Suit.SPADE, R.drawable.jack_of_spades),
    QUEEN_OF_DIAMONDS(Rank.QUEEN, Suit.DIAMOND, R.drawable.queen_of_diamonds),
    QUEEN_OF_CLUBS(Rank.QUEEN, Suit.CLUB, R.drawable.queen_of_clubs),
    QUEEN_OF_HEARTS(Rank.QUEEN, Suit.HEART, R.drawable.queen_of_hearts),
    QUEEN_OF_SPADES(Rank.QUEEN, Suit.SPADE, R.drawable.queen_of_spades),
    KING_OF_DIAMONDS(Rank.KING, Suit.DIAMOND, R.drawable.king_of_diamonds),
    KING_OF_CLUBS(Rank.KING, Suit.CLUB, R.drawable.king_of_clubs),
    KING_OF_HEARTS(Rank.KING, Suit.HEART, R.drawable.king_of_hearts),
    KING_OF_SPADES(Rank.KING, Suit.SPADE, R.drawable.king_of_spades),
    ACE_OF_DIAMONDS(Rank.ACE, Suit.DIAMOND, R.drawable.ace_of_diamonds),
    ACE_OF_CLUBS(Rank.ACE, Suit.CLUB, R.drawable.ace_of_clubs),
    ACE_OF_HEARTS(Rank.ACE, Suit.HEART, R.drawable.ace_of_hearts),
    ACE_OF_SPADES(Rank.ACE, Suit.SPADE, R.drawable.ace_of_spades);
    
    enum class Suit {
        DIAMOND, CLUB, HEART, SPADE
    }

    enum class Rank(val value: Int) {
        TWO(2),
        THREE(3),
        FOUR(4),
        FIVE(5),
        SIX(6),
        SEVEN(7),
        EIGHT(8),
        NINE(9),
        TEN(10),
        JACK(10),
        QUEEN(10),
        KING(10),
        ACE(11)
    }
}