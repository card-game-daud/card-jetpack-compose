package id.bootcamp.card.domain.model

data class AnonymousUserModel(
    val id: String,
    var name: String
)
