package id.bootcamp.card.domain.usecase

import id.bootcamp.card.domain.model.AnonymousUserModel
import id.bootcamp.card.domain.repository.AuthRepository

class GetAnonymousUserUseCase(private val authRepository: AuthRepository) {
    suspend operator fun invoke(): AnonymousUserModel? {
        val currentUser = authRepository.getCurrentUser()
        return currentUser ?: authRepository.signInAnonymously()
    }
}