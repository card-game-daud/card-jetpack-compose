package id.bootcamp.card.domain.usecase

import id.bootcamp.card.domain.mapper.rotateToRemote
import id.bootcamp.card.domain.model.PlayerDeck
import id.bootcamp.card.domain.repository.OnlinePlayRepository

class EndOnlineGameUseCase(
    private val onlinePlayRepository: OnlinePlayRepository,
    private val calculateDeckScoreUseCase: CalculateDeckScoreUseCase
) {
    operator fun invoke(gameId: String, decksArray: Array<PlayerDeck>) {
        updateScoresAndGameStatus(gameId, decksArray) { it }
    }

    operator fun invoke(gameId: String, decksArray: Array<PlayerDeck>, userRelativeId: Int) {
        updateScoresAndGameStatus(gameId, decksArray) { it.rotateToRemote(userRelativeId) }
    }

    private fun updateScoresAndGameStatus(
        gameId: String,
        decksArray: Array<PlayerDeck>,
        positionAdjuster: (Array<Int>) -> Array<Int>
    ) {
        val allPlayersScore = Array(4){
            calculateDeckScoreUseCase(decksArray[it].listCard)
        }
        onlinePlayRepository.endGame(gameId, positionAdjuster(allPlayersScore))
    }
}