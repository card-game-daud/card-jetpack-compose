package id.bootcamp.card.domain.usecase

import androidx.compose.runtime.snapshots.SnapshotStateList
import id.bootcamp.card.domain.model.CardModel
import id.bootcamp.card.domain.model.PlayerDeck

class GenerateDeckUseCase {
    operator fun invoke(
        numPlayers: Int,
        numCardsPerDeck: Int
    ): Pair<Array<PlayerDeck>, SnapshotStateList<CardModel>> {
        val shuffledCards = CardModel.values()
        shuffledCards.shuffle()

        val newPlayersDeck = Array(numPlayers) { i: Int ->
            val listCard = SnapshotStateList<CardModel>()
            shuffledCards.copyOfRange(
                i*numCardsPerDeck,
                i*numCardsPerDeck + numCardsPerDeck
            ).forEach { listCard.add(it) }

            PlayerDeck(i, listCard)
        }

        val newDrawPile = SnapshotStateList<CardModel>()
        shuffledCards.copyOfRange(
            numPlayers*numCardsPerDeck,
            shuffledCards.size
        ).forEach { newDrawPile.add(it) }

        return newPlayersDeck to newDrawPile
    }
}