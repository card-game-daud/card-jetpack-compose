package id.bootcamp.card.domain.model

data class PlayerModel(
    var name: String = "",
    var isHuman: Boolean = false
)
