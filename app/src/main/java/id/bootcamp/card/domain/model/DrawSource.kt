package id.bootcamp.card.domain.model

enum class DrawSource {
    DRAW_PILE, DISCARD_PILE
}