package id.bootcamp.card.domain.mapper

import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DataSnapshot
import id.bootcamp.card.domain.model.AnonymousUserModel

fun FirebaseUser.toDomainModel(): AnonymousUserModel {
    return AnonymousUserModel(this.uid, this.displayName ?: this.uid)
}

fun DataSnapshot.fromLobbyToUserModel(): AnonymousUserModel {
    return AnonymousUserModel(
        this.child("id").getValue(String::class.java)!!,
        this.child("name").getValue(String::class.java)!!
    )
}

fun DataSnapshot.fromOngoingToUserModel(): AnonymousUserModel {
    return AnonymousUserModel(
        this.key!!,
        this.child("name").getValue(String::class.java)!!
    )
}