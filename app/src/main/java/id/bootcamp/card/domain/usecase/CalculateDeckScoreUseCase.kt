package id.bootcamp.card.domain.usecase

import id.bootcamp.card.domain.model.CardModel

class CalculateDeckScoreUseCase {
    operator fun invoke(listCard: List<CardModel>): Int {
        var maxScore = Int.MIN_VALUE
        for (suit in CardModel.Suit.values()) {
            var score = 0
            for (card in listCard) {
                if (card.suit == suit)
                    score += card.rank.value
                else
                    score -= card.rank.value
            }
            if (score > maxScore)
                maxScore = score
        }
        return maxScore
    }
}