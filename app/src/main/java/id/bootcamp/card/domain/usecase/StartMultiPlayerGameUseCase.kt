package id.bootcamp.card.domain.usecase

import id.bootcamp.card.domain.model.PlayGameModel
import id.bootcamp.card.domain.repository.LobbyRepository

class StartMultiPlayerGameUseCase(
    private val lobbyRepository: LobbyRepository,
    private val startOnlineGameUseCase: StartOnlineGameUseCase
) {
    suspend operator fun invoke(gameId: String, numPlayers: Int, numCardsPerDeck: Int): PlayGameModel? {
        val game = lobbyRepository.getGameById(gameId)
        return if (game != null) {
            startOnlineGameUseCase(game, numPlayers, numCardsPerDeck)
        } else {
            null
        }
    }
}