package id.bootcamp.card.domain.usecase

import id.bootcamp.card.domain.model.CardModel
import id.bootcamp.card.domain.model.DrawSource
import id.bootcamp.card.domain.model.PlayerDeck
import id.bootcamp.card.domain.model.PlayerDiscardPile

class ComputerMoveUseCase {
    operator fun invoke(playerDeck: PlayerDeck, playerDiscardPile: PlayerDiscardPile): Pair<DrawSource, CardModel> {
        return computerMoveRandom(playerDeck, playerDiscardPile)
    }

    // Put this into its own function in case new type of computer move is added
    private fun computerMoveRandom(playerDeck: PlayerDeck, playerDiscardPile: PlayerDiscardPile): Pair<DrawSource, CardModel> {
        val drawSource = if (playerDiscardPile.listCard.isEmpty()) {
            DrawSource.DRAW_PILE
        } else {
            DrawSource.values().random()
        }
        val discardedCard = playerDeck.listCard.random()
        return drawSource to discardedCard
    }
}