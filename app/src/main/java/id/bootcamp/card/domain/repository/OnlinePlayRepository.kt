package id.bootcamp.card.domain.repository

import androidx.compose.runtime.snapshots.SnapshotStateList
import id.bootcamp.card.domain.model.CardModel
import id.bootcamp.card.domain.model.GameStatus
import id.bootcamp.card.domain.model.LobbyGameModel
import id.bootcamp.card.domain.model.PlayGameModel
import id.bootcamp.card.domain.model.PlayerDeck
import id.bootcamp.card.domain.model.PlayerDiscardPile

interface OnlinePlayRepository {
    fun syncGameData(
        gameId: String,
        callback: (PlayGameModel) -> Unit
    )

    fun checkConnectionStatus(callback: (Boolean) -> Unit)

    suspend fun startGame(
        lobbyGameData: LobbyGameModel?,
        startingDrawPile: List<CardModel>,
        startingPlayersDecks: Array<PlayerDeck>,
        playerIdRange: IntRange
    ): PlayGameModel

    fun enterGame(gameId: String, userId: String, callback: (Int) -> Unit)
    fun drawFromDrawPile(
        updatedPlayersDeck: Array<PlayerDeck>,
        updatedDrawPile: SnapshotStateList<CardModel>,
        updatedPlayerId: Int,
        onFinish: () -> Unit
    )

    fun drawFromDiscardPile(
        updatedPlayersDeck: Array<PlayerDeck>,
        updatedDiscardPiles: Array<PlayerDiscardPile>,
        updatedPlayerId: Int,
        onFinish: () -> Unit
    )

    fun discardCard(
        updatedPlayersDeck: Array<PlayerDeck>,
        updatedDiscardPiles: Array<PlayerDiscardPile>,
        onFinish: () -> Unit
    )

    fun endTurn(currentPlayerId: Int, callback: () -> Unit)
    fun updateGameStatus(newGameStatus: GameStatus)
    fun endGame(gameId: String, scores: Array<Int>)
    fun leaveGame(gameId: String, userId: String)

    suspend fun getCurrentGameOfUser(userId: String): LobbyGameModel?

    suspend fun getGameById(gameId: String): LobbyGameModel?
}