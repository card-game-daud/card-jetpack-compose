package id.bootcamp.card.domain.mapper

import id.bootcamp.card.domain.model.PlayerDeck
import java.util.Collections

@Suppress("UNCHECKED_CAST")
inline fun <reified T> Array<T>.rotateFromRemote(userRelativeId: Int): Array<T> {
    val list = this.toList()
    Collections.rotate(list, -userRelativeId)
    if (this.isArrayOf<PlayerDeck>()) {
        val decksWithRotatedIds = (list as List<PlayerDeck>).map {
            PlayerDeck(it.id.rotateFromRemote(userRelativeId), it.listCard)
        }
        Collections.copy(list as MutableList<PlayerDeck>, decksWithRotatedIds)
    }
    return list.toTypedArray()
}

@Suppress("UNCHECKED_CAST")
inline fun <reified T> Array<T>.rotateToRemote(userRelativeId: Int): Array<T> {
    val list = this.toList()
    Collections.rotate(list, userRelativeId)
    if (this.isArrayOf<PlayerDeck>()) {
        val decksWithRotatedIds = (list as List<PlayerDeck>).map {
            PlayerDeck(it.id.rotateToRemote(userRelativeId), it.listCard)
        }
        Collections.copy(list as MutableList<PlayerDeck>, decksWithRotatedIds)
    }
    return list.toTypedArray()
}

fun Int.rotateFromRemote(userRelativeId: Int): Int {
    return (this - userRelativeId + 4) % 4
}

fun Int.rotateToRemote(userRelativeId: Int): Int {
    return (this + userRelativeId) % 4
}