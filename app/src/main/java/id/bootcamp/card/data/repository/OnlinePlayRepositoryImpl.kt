package id.bootcamp.card.data.repository

import android.util.Log
import androidx.compose.runtime.snapshots.SnapshotStateList
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import id.bootcamp.card.data.MainKartuDatabase
import id.bootcamp.card.data.getDrawPile
import id.bootcamp.card.data.getGameStatus
import id.bootcamp.card.data.getPlayersDeck
import id.bootcamp.card.data.getPlayersDiscardPile
import id.bootcamp.card.data.getPlayersScore
import id.bootcamp.card.data.getPlayersTurnStatus
import id.bootcamp.card.domain.mapper.fromOngoingSnapshotToGameModel
import id.bootcamp.card.domain.mapper.fromOngoingSnapshotToGameModelList
import id.bootcamp.card.domain.model.CardModel
import id.bootcamp.card.domain.model.GameStatus
import id.bootcamp.card.domain.model.LobbyGameModel
import id.bootcamp.card.domain.model.PlayGameModel
import id.bootcamp.card.domain.model.PlayerDeck
import id.bootcamp.card.domain.model.PlayerDiscardPile
import id.bootcamp.card.domain.model.TurnStatus
import id.bootcamp.card.domain.repository.OnlinePlayRepository
import id.bootcamp.card.utils.GameDefaults
import id.bootcamp.card.utils.toSnapshotStateList
import kotlinx.coroutines.tasks.await

class OnlinePlayRepositoryImpl private constructor(database: FirebaseDatabase) : OnlinePlayRepository {
    private val games = database.getReference("games")
    private var game = games.child(GameDefaults.ID) // dummy
    private var syncGameDataListener: ValueEventListener? = null

    override fun syncGameData(
        gameId: String,
        callback: (PlayGameModel) -> Unit
    ) {
        if (gameId.isNotEmpty()) {
            game = games.child(gameId)
            syncGameDataListener = game.addValueEventListener(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    val gameData = getGameData(snapshot)
                    if (gameData != null) callback(gameData)
                }

                override fun onCancelled(error: DatabaseError) {
                    Log.d("sync_failure", error.message)
                }
            })
        }
    }

    private fun getGameData(snapshot: DataSnapshot): PlayGameModel? {
        val drawPile = snapshot.child("draw_pile").getDrawPile()
        val playersDeck = snapshot.child("players").getPlayersDeck()
        val playersDiscardPile = snapshot.child("players").getPlayersDiscardPile()
        if (drawPile.size + playersDeck.sumOf { it.listCard.size } + playersDiscardPile.sumOf { it.listCard.size } != 52) {
            // For some reason, draw pile is often not updated properly when drawing,
            //  creating a duplicate card from the leftover card
            // This is the workaround
            if (
                drawPile.lastOrNull() in playersDeck.flatMap { it.listCard }
                || drawPile.lastOrNull() in playersDiscardPile.flatMap { it.listCard }
            ) {
                game.child("draw_pile").setValue(drawPile.dropLast(1))
            }
            return null
        } else {
            return (
                PlayGameModel(
                    drawPile,
                    playersDeck,
                    playersDiscardPile,
                    snapshot.child("players").getPlayersTurnStatus(),
                    snapshot.child("game_status").getGameStatus(),
                    snapshot.child("players").getPlayersScore(),
                    snapshot.child("current_player_id").getValue(Int::class.java)
                )
            )
        }
    }

    override fun checkConnectionStatus(callback: (Boolean) -> Unit) {
        games.parent!!.child(".info/connected").addValueEventListener(object: ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val isConnected = snapshot.getValue(Boolean::class.java) ?: false
                if (isConnected) {
                    Log.d("update_connection", "connected")
                } else {
                    Log.d("update_connection", "not connected")
                }
                callback(isConnected)
            }

            override fun onCancelled(error: DatabaseError) {
                Log.w("update_connection", error.message)
            }
        })
    }

    override suspend fun startGame(
        lobbyGameData: LobbyGameModel?,
        startingDrawPile: List<CardModel>,
        startingPlayersDecks: Array<PlayerDeck>,
        playerIdRange: IntRange
    ): PlayGameModel {
        val startGameData = createStartGameData(lobbyGameData, startingDrawPile, startingPlayersDecks, playerIdRange)

        var writtenData: PlayGameModel? = null
        if (lobbyGameData == null) {
            writtenData = updateDatabaseWithStartGameData(GameDefaults.ID, startGameData, "generated default ID")
        }
        if (!lobbyGameData?.id.isNullOrEmpty()) {
            writtenData = updateDatabaseWithStartGameData(lobbyGameData!!.id, startGameData, "success")
        }

        // If writtenData is null because the coroutine is cancelled or for other reasons,
        // create the return data manually
        if (writtenData == null) {
            writtenData = PlayGameModel(
                drawPile = startingDrawPile.toSnapshotStateList(),
                playersDecks = startingPlayersDecks,
                playersDiscardPiles = Array(playerIdRange.last + 1) {
                    PlayerDiscardPile(it, SnapshotStateList())
                },
                playersTurnStatuses = Array(playerIdRange.last + 1) {
                    if (it == startGameData["current_player_id"])
                        TurnStatus.DRAWING
                    else TurnStatus.WAITING
                },
                gameStatus = GameStatus.ONGOING,
                playersScores = Array(playerIdRange.last + 1) { 0 },
                currentPlayerId = startGameData["current_player_id"] as Int
            )
        }
        return writtenData
    }

    private fun createStartGameData(
        lobbyGameData: LobbyGameModel?,
        startingDrawPile: List<CardModel>,
        startingPlayersDecks: Array<PlayerDeck>,
        playerIdRange: IntRange,
    ): Map<String, Any?> {
        val startGameData = mutableMapOf<String, Any?>()
        startGameData["draw_pile"] = startingDrawPile
        startGameData["game_status"] = GameStatus.ONGOING.ordinal
        startGameData["current_player_id"] = playerIdRange.random()
        val playersData = mutableMapOf<String, Any?>()
        for (id in playerIdRange) {
            val playerData = mutableMapOf<String, Any?>()
            playerData["id"] = id
            playerData["name"] = lobbyGameData?.listPlayers?.getOrNull(id)?.name ?: "COMP $id"
            playerData["deck"] = startingPlayersDecks[id].listCard
            playerData["discard_pile"] = null
            playerData["turn_status"] = if (id == startGameData["current_player_id"])
                TurnStatus.DRAWING.ordinal
            else TurnStatus.WAITING.ordinal
            playerData["score"] = null
            playersData[lobbyGameData?.listPlayersId?.getOrNull(id) ?: "COMP $id"] = playerData
        }
        startGameData["players"] = playersData
        return startGameData
    }

    private suspend fun updateDatabaseWithStartGameData(
        gameId: String, startGameData: Map<String, Any?>, successMsg: String
    ): PlayGameModel? = try {
        game = games.child(gameId)
        game.updateChildren(startGameData).await()
        val gameData = getGameData(game.get().await())
        Log.d("start_game", if (gameData != null) successMsg else "received null")
        gameData
    } catch (e: Exception) {
        Log.w("start_game", e.message ?: "failed")
        null
    }

    override fun enterGame(gameId: String, userId: String, callback: (Int) -> Unit) {
        if (gameId.isNotEmpty()) {
            game = games.child(gameId)
            game.child("entered_humans").child(userId).setValue("entered").addOnSuccessListener {
                game.child("entered_humans").get().addOnSuccessListener {
                    callback(it.childrenCount.toInt())
                }
            }
        }
    }

    override fun drawFromDrawPile(
        updatedPlayersDeck: Array<PlayerDeck>,
        updatedDrawPile: SnapshotStateList<CardModel>,
        updatedPlayerId: Int,
        onFinish: () -> Unit
    ) {
        val updateData = mutableMapOf<String, Any?>()
        game.child("players").get().addOnSuccessListener {
            it.children.forEach { player ->
                val id = player.child("id").getValue(Int::class.java)
                if (id != null) {
                    updateData["players/${player.key}/deck"] = updatedPlayersDeck[id].listCard
                    if (id == updatedPlayerId) {
                        updateData["players/${player.key}/turn_status"] = TurnStatus.DISCARDING.ordinal
                    }
                }
            }
            updateData["draw_pile"] = updatedDrawPile
            game.updateChildren(updateData).addOnSuccessListener { onFinish() }
        }
    }

    override fun drawFromDiscardPile(
        updatedPlayersDeck: Array<PlayerDeck>,
        updatedDiscardPiles: Array<PlayerDiscardPile>,
        updatedPlayerId: Int,
        onFinish: () -> Unit
    ) {
        val updateData = mutableMapOf<String, Any?>()
        game.child("players").get().addOnSuccessListener {
            it.children.forEach { player ->
                val id = player.child("id").getValue(Int::class.java)
                if (id != null) {
                    updateData["players/${player.key}/discard_pile"] = updatedDiscardPiles[id].listCard
                    updateData["players/${player.key}/deck"] = updatedPlayersDeck[id].listCard
                    if (id == updatedPlayerId) {
                        updateData["players/${player.key}/turn_status"] = TurnStatus.DISCARDING.ordinal
                    }
                }
            }
            game.updateChildren(updateData).addOnSuccessListener { onFinish() }
        }
    }

    override fun discardCard(
        updatedPlayersDeck: Array<PlayerDeck>,
        updatedDiscardPiles: Array<PlayerDiscardPile>,
        onFinish: () -> Unit
    ) {
        val updatedPlayersData = mutableMapOf<String, Any?>()
        game.child("players").get().addOnSuccessListener {
            it.children.forEach { player ->
                val id = player.child("id").getValue(Int::class.java)
                if (id != null) {
                    updatedPlayersData["${player.key}/deck"] = updatedPlayersDeck[id].listCard
                    updatedPlayersData["${player.key}/discard_pile"] = updatedDiscardPiles[id].listCard
                }
            }
            game.child("players").updateChildren(updatedPlayersData).addOnSuccessListener { onFinish() }
        }
    }

    override fun endTurn(currentPlayerId: Int, callback: () -> Unit) {
        val updatedGameData = mutableMapOf<String, Any?>()
        updatedGameData["current_player_id"] = (currentPlayerId + 1) % 4
        game.child("players").get().addOnSuccessListener {
            it.children.forEach { player ->
                val id = player.child("id").getValue(Int::class.java)
                if (id != null) {
                    updatedGameData["players/${player.key}/turn_status"] = if (id == (currentPlayerId + 1) % 4) {
                        TurnStatus.DRAWING.ordinal
                    } else {
                        TurnStatus.WAITING.ordinal
                    }
                }
            }
            game.updateChildren(updatedGameData).addOnSuccessListener {
                callback()
            }
        }
    }

    override fun updateGameStatus(newGameStatus: GameStatus) {
        game.child("game_status").setValue(newGameStatus.ordinal)
    }

    override fun endGame(gameId: String, scores: Array<Int>) {
        if (gameId.isNotEmpty()) {
            game = games.child(gameId)
            game.child("players").get().addOnSuccessListener {
                val updatedGameData = mutableMapOf<String, Any?>()
                updatedGameData["game_status"] = GameStatus.FINISHING.ordinal

                it.children.forEach { player ->
                    val id = player.child("id").getValue(Int::class.java)
                    if (id != null) {
                        updatedGameData["players/${player.key}/score"] = scores[id]
                    }
                }

                game.updateChildren(updatedGameData).addOnSuccessListener {
                    Log.d("end_game", "end game successful")
                }
            }
        }
    }

    override fun leaveGame(gameId: String, userId: String) {
        if (gameId.isNotEmpty()) {
            game = games.child(gameId)
            game.child("entered_humans").get().addOnSuccessListener {
                // If there is only one player left, remove the whole game from database
                if (it.childrenCount <= 1) {
                    game.removeValue()
                } else {
                    it.ref.child(userId).removeValue()
                }
            }
        }
    }

    override suspend fun getCurrentGameOfUser(userId: String): LobbyGameModel? = try {
        val gameList = games.get().await()
        Log.d("get_current_user_game", "success")
        gameList.fromOngoingSnapshotToGameModelList(userId).getOrNull(0)
    } catch (e: Exception) {
        Log.w("get_current_user_game", e.message ?: "")
        null
    }

    override suspend fun getGameById(gameId: String): LobbyGameModel? = try {
        val gameSnapshot = games.child(gameId).get().await()
        Log.d("get_ongoing_game", "success")
        gameSnapshot.fromOngoingSnapshotToGameModel()
    } catch (e: Exception) {
        Log.w("get_ongoing_game", e.message ?: "failed")
        null
    }

    companion object {
        @Volatile
        private var instance: OnlinePlayRepositoryImpl? = null
        fun getInstance(): OnlinePlayRepositoryImpl = instance ?: synchronized(this) {
            instance ?: OnlinePlayRepositoryImpl(
                MainKartuDatabase.getInstance()
            )
        }
    }
}