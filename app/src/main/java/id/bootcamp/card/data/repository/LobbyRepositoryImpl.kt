package id.bootcamp.card.data.repository

import android.util.Log
import androidx.compose.runtime.snapshots.SnapshotStateList
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import id.bootcamp.card.data.MainKartuDatabase
import id.bootcamp.card.domain.mapper.fromLobbySnapshotToGameModel
import id.bootcamp.card.domain.mapper.fromLobbySnapshotToGameModelList
import id.bootcamp.card.domain.model.AnonymousUserModel
import id.bootcamp.card.domain.model.LobbyGameModel
import id.bootcamp.card.domain.repository.LobbyRepository
import kotlinx.coroutines.tasks.await

class LobbyRepositoryImpl private constructor(database: FirebaseDatabase) : LobbyRepository {
    private val lobby = database.getReference("lobby")

    override fun getLobbyData(callback: (SnapshotStateList<LobbyGameModel>) -> Unit) {
        lobby.addValueEventListener(object: ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                callback(snapshot.fromLobbySnapshotToGameModelList())
            }

            override fun onCancelled(error: DatabaseError) {
                Log.w("lobby_sync_fail", error.message)
            }
        })
    }

    override fun checkConnectionStatus(callback: (Boolean) -> Unit) {
        lobby.parent!!.child(".info/connected").addValueEventListener(object: ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val isConnected = snapshot.getValue(Boolean::class.java) ?: false
                if (isConnected) {
                    Log.d("update_connection", "connected")
                } else {
                    Log.d("update_connection", "not connected")
                }
                callback(isConnected)
            }

            override fun onCancelled(error: DatabaseError) {
                Log.w("update_connection", error.message)
            }
        })
    }

    override fun addGame(user: AnonymousUserModel) {
        val newGame = lobby.push()
        val newGameData = mapOf(
            "is_waiting" to true,
            "player_host" to mapOf(
                "id" to user.id,
                "name" to user.name
            )
        )
        newGame.setValue(newGameData)
    }

    override fun joinGame(user: AnonymousUserModel, gameId: String) {
        lobby.child(gameId).get().addOnSuccessListener {
            val playerCount = it.childrenCount - 1
            if (playerCount < 4) {
                it.ref.child("player${playerCount}").setValue(mapOf(
                    "id" to user.id,
                    "name" to user.name
                ))
            }
        }
    }

    override fun leaveGame(userId: String, gameId: String) {
        lobby.child(gameId).get().addOnSuccessListener { game ->
            game.children.filterNot { it.key == "is_waiting" }.forEach { player ->
                if (player.child("id").getValue(String::class.java) == userId) {
                    if (player.key == "player_host") {
                        game.ref.removeValue()
                    } else {
                        player.ref.removeValue()
                    }
                }
            }
        }
    }

    override fun startGame(gameId: String) {
        lobby.child(gameId).child("is_waiting").setValue(false)
    }

    override suspend fun getGameById(id: String): LobbyGameModel? = try {
        if (id.isNotEmpty()) {
            val gameSnapshot = lobby.child(id).get().await()
            Log.d("lobby_get_game", "success")
            gameSnapshot.fromLobbySnapshotToGameModel()
        } else {
            Log.w("lobby_get_game", "failed: id is empty")
            null
        }
    } catch (e: Exception) {
        Log.w("lobby_get_game", "failed: ${e.message}")
        null
    }

    override fun removeGame(gameId: String) {
        lobby.child(gameId).removeValue()
    }

    companion object {
        @Volatile
        private var instance: LobbyRepositoryImpl? = null
        fun getInstance(): LobbyRepositoryImpl = instance ?: synchronized(this) {
            instance ?: LobbyRepositoryImpl(
                MainKartuDatabase.getInstance()
            )
        }
    }
}