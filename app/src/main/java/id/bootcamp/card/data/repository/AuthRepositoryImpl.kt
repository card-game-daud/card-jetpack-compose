package id.bootcamp.card.data.repository

import android.util.Log
import com.google.firebase.Firebase
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.auth
import com.google.firebase.auth.userProfileChangeRequest
import id.bootcamp.card.domain.mapper.toDomainModel
import id.bootcamp.card.domain.model.AnonymousUserModel
import id.bootcamp.card.domain.repository.AuthRepository
import kotlinx.coroutines.tasks.await

class AuthRepositoryImpl private constructor(private val auth: FirebaseAuth) : AuthRepository {

    override fun getCurrentUser(): AnonymousUserModel? {
        return auth.currentUser?.toDomainModel()
    }

    override suspend fun signInAnonymously(): AnonymousUserModel? = try {
        auth.signInAnonymously().await()
        Log.d("AnonymousAuth", "signInAnonymously:success")
        auth.currentUser?.toDomainModel()
    } catch (e: Exception) {
        Log.w("AnonymousAuth", "signInAnonymously:failure", e)
        null
    }

    override suspend fun updateUserName(newName: String): AnonymousUserModel? = try {
        val updateNameRequest = userProfileChangeRequest {
            displayName = newName
        }
        auth.currentUser?.updateProfile(updateNameRequest)?.await()
        Log.d("AnonymousAuth", "updateUserName:success")
        auth.currentUser?.toDomainModel()
    } catch (e: Exception) {
        Log.w("AnonymousAuth", "updateUserName:failure", e)
        null
    }

    companion object {
        @Volatile
        private var instance: AuthRepositoryImpl? = null
        fun getInstance(): AuthRepositoryImpl = instance ?: synchronized(this) {
            instance ?: AuthRepositoryImpl(
                Firebase.auth
            )
        }.also { instance = it }
    }
}