package id.bootcamp.card.data.datastore

import android.content.Context
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.emptyPreferences
import androidx.datastore.preferences.core.intPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import id.bootcamp.card.ui.theme.ChosenTheme
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import java.io.IOException

val Context.dataStore by preferencesDataStore(name = "settings")

class MainDataStore(context: Context) {
    private object DataStoreKey {
        val DARK_THEME = intPreferencesKey("dark_theme")
    }

    private val dataStore = context.dataStore

    suspend fun changeTheme(chosenTheme: ChosenTheme) {
        dataStore.edit { settings ->
            settings[DataStoreKey.DARK_THEME] = chosenTheme.ordinal
        }
    }

    fun loadTheme(): Flow<ChosenTheme?> {
        return dataStore.data
            .catch { exception -> if (exception is IOException) emptyPreferences() else throw exception }
            .map { settings ->
                ChosenTheme.fromOrdinal(settings[DataStoreKey.DARK_THEME])
            }
    }


}