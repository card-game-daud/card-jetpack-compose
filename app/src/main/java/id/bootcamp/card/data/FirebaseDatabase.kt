package id.bootcamp.card.data

import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.database
import com.google.firebase.Firebase

abstract class MainKartuDatabase {
    companion object {
        @Volatile
        private var instance: FirebaseDatabase? = null
        fun getInstance(): FirebaseDatabase = instance ?: synchronized(this) {
            instance ?: Firebase.database("https://main-kartu-default-rtdb.asia-southeast1.firebasedatabase.app/")
        }.also { instance = it }
    }
}