package id.bootcamp.card.data

import androidx.compose.runtime.snapshots.SnapshotStateList
import com.google.firebase.database.DataSnapshot
import id.bootcamp.card.domain.model.CardModel
import id.bootcamp.card.domain.model.GameStatus
import id.bootcamp.card.domain.model.PlayerDeck
import id.bootcamp.card.domain.model.PlayerDiscardPile
import id.bootcamp.card.domain.model.TurnStatus

fun DataSnapshot.getId(): Int? {
    return this.child("id").getValue(Int::class.java)
}

fun DataSnapshot.getGameStatus(): GameStatus? {
    val gameStatusOrdinal = this.getValue(Int::class.java)
    return if (gameStatusOrdinal != null)
        GameStatus.values()[gameStatusOrdinal]
    else null
}

fun DataSnapshot.getDrawPile(): SnapshotStateList<CardModel> {
    val drawPile = this.children.mapNotNull { it.getValue(CardModel::class.java) }
    val drawPileState = SnapshotStateList<CardModel>()
    drawPileState.addAll(drawPile)
    return drawPileState
}

fun DataSnapshot.getPlayersDeck(): Array<PlayerDeck> {
    val playersDeckList = mutableListOf<PlayerDeck>()
    this.children.sortedBy { it.getId() }.forEach { player ->
        val id = player.child("id").getValue(Int::class.java)
        val deck = player.child("deck").children.mapNotNull { it.getValue(CardModel::class.java) }
        val deckState = SnapshotStateList<CardModel>()
        deckState.addAll(deck)
        if (id != null) {
            playersDeckList.add(PlayerDeck(id, deckState))
        }
    }
    return playersDeckList.toTypedArray()
}

fun DataSnapshot.getPlayersDiscardPile(): Array<PlayerDiscardPile> {
    val playersDeckList = mutableListOf<PlayerDiscardPile>()
    this.children.sortedBy { it.getId() }.forEach { player ->
        val id = player.child("id").getValue(Int::class.java)
        val discardPile = player.child("discard_pile").children.mapNotNull { it.getValue(CardModel::class.java) }
        val discardPileState = SnapshotStateList<CardModel>()
        discardPileState.addAll(discardPile)
        if (id != null) {
            playersDeckList.add(PlayerDiscardPile(id, discardPileState))
        }
    }
    return playersDeckList.toTypedArray()
}

fun DataSnapshot.getPlayersTurnStatus(): Array<TurnStatus> {
    val turnStatusList = mutableListOf<TurnStatus>()
    this.children.sortedBy { it.getId() }.forEach { player ->
        val id = player.child("id").getValue(Int::class.java)
        val turnStatusOrdinal = player.child("turn_status").getValue(Int::class.java)
        if (id != null && turnStatusOrdinal != null) {
            turnStatusList.add(TurnStatus.values()[turnStatusOrdinal])
        }
    }
    return turnStatusList.toTypedArray()
}

fun DataSnapshot.getPlayersScore(): Array<Int> {
    val playersScoreList = mutableListOf<Int>()
    this.children.sortedBy { it.getId() }.forEach { player ->
        val score = player.child("score").getValue(Int::class.java)
        if (score != null) playersScoreList.add(score)
    }
    return playersScoreList.toTypedArray()
}