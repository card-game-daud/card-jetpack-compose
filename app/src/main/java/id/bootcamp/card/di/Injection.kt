package id.bootcamp.card.di

import id.bootcamp.card.data.repository.AuthRepositoryImpl
import id.bootcamp.card.data.repository.LobbyRepositoryImpl
import id.bootcamp.card.data.repository.OnlinePlayRepositoryImpl
import id.bootcamp.card.domain.usecase.CalculateDeckScoreUseCase
import id.bootcamp.card.domain.usecase.ChangeUserNameUseCase
import id.bootcamp.card.domain.usecase.ComputerMoveUseCase
import id.bootcamp.card.domain.usecase.EndOnlineGameUseCase
import id.bootcamp.card.domain.usecase.EnterLobbyUseCase
import id.bootcamp.card.domain.usecase.EnterMultiPlayerGameUseCase
import id.bootcamp.card.domain.usecase.GenerateDeckUseCase
import id.bootcamp.card.domain.usecase.GeneratePlayersArrayUseCase
import id.bootcamp.card.domain.usecase.GetAnonymousUserUseCase
import id.bootcamp.card.domain.usecase.GetCurrentGameUseCase
import id.bootcamp.card.domain.usecase.StartMultiPlayerGameUseCase
import id.bootcamp.card.domain.usecase.StartOnlineGameUseCase

object Injection {
    private fun provideCalculateDeckScoreUseCase() = CalculateDeckScoreUseCase()

    fun provideGenerateDeckUseCase() = GenerateDeckUseCase()

    fun provideComputerMoveUseCase() = ComputerMoveUseCase()

    fun provideGetAnonymousUserUseCase(): GetAnonymousUserUseCase {
        return GetAnonymousUserUseCase(AuthRepositoryImpl.getInstance())
    }

    fun provideChangeUserNameUseCase(): ChangeUserNameUseCase {
        return ChangeUserNameUseCase(AuthRepositoryImpl.getInstance())
    }

    fun provideEnterLobbyUseCase(): EnterLobbyUseCase {
        return EnterLobbyUseCase(
            provideGetAnonymousUserUseCase(),
            LobbyRepositoryImpl.getInstance()
        )
    }

    fun provideStartOnlineGameUseCase(): StartOnlineGameUseCase {
        return StartOnlineGameUseCase(
            OnlinePlayRepositoryImpl.getInstance(),
            provideGenerateDeckUseCase(),
            provideGetAnonymousUserUseCase()
        )
    }

    fun provideStartMultiPlayerGameUseCase(): StartMultiPlayerGameUseCase {
        return StartMultiPlayerGameUseCase(
            LobbyRepositoryImpl.getInstance(),
            provideStartOnlineGameUseCase()
        )
    }

    fun provideEnterMultiPlayerGameUseCase(): EnterMultiPlayerGameUseCase {
        return EnterMultiPlayerGameUseCase(
            OnlinePlayRepositoryImpl.getInstance(),
            LobbyRepositoryImpl.getInstance()
        )
    }

    fun provideGeneratePlayersArrayUseCase(): GeneratePlayersArrayUseCase {
        return GeneratePlayersArrayUseCase()
    }

    fun provideEndOnlineGameUseCase(): EndOnlineGameUseCase {
        return EndOnlineGameUseCase(
            OnlinePlayRepositoryImpl.getInstance(),
            provideCalculateDeckScoreUseCase()
        )
    }

    fun provideGetCurrentGameUseCase(): GetCurrentGameUseCase {
        return GetCurrentGameUseCase(OnlinePlayRepositoryImpl.getInstance())
    }
}